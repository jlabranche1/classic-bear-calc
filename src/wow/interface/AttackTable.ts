import { strict as assert } from 'assert'

// https://bookdown.org/marrowwar/marrow_compendium/mechanics.html#glancing_blows
// Glancing blows do 35% reduced damage
const GLANCING_BLOW_MODIFIER = 0.35
// Extra damage recieved from a crushing blow
const CRUSHING_BLOW_MODIFIER = 0.5

interface AttackTableConstructorInputs {
  miss: number
  dodge: number
  parry: number
  glancingBlow: number
  block: number
  criticalHit: number
  crushingBlow: number
}

export default class AttackTable {
  miss: number
  dodge: number
  parry: number
  glancingBlow: number
  block: number
  criticalHit: number
  crushingBlow: number

  constructor({ miss, dodge, parry, glancingBlow, block, criticalHit, crushingBlow }: AttackTableConstructorInputs) {
    this.miss = miss
    this.dodge = dodge
    this.parry = parry
    this.glancingBlow = glancingBlow
    this.block = block
    this.criticalHit = Math.max(Math.min(1 - miss - dodge - parry - glancingBlow - block, criticalHit), 0)
    this.crushingBlow = Math.max(
      Math.min(1 - miss - dodge - parry - glancingBlow - block - this.criticalHit, crushingBlow),
      0
    )
    const attackTableSum =
      this.miss +
      this.dodge +
      this.parry +
      this.glancingBlow +
      this.block +
      this.criticalHit +
      this.crushingBlow +
      this.ordinaryHit
    assert(Math.abs(attackTableSum - 1) < 0.001, `Expected Attack Table to sum to 1. Got ${attackTableSum}`)
  }

  get ordinaryHit(): number {
    return Math.max(
      1 - this.miss - this.dodge - this.parry - this.glancingBlow - this.block - this.criticalHit - this.crushingBlow,
      0
    )
  }

  get attackModifier(): number {
    return (
      1 -
      this.dodge -
      this.miss -
      this.block -
      this.parry +
      this.criticalHit -
      GLANCING_BLOW_MODIFIER * this.glancingBlow +
      this.crushingBlow * CRUSHING_BLOW_MODIFIER
    )
  }
}
