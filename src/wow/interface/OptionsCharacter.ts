import Gender from '../enum/Gender'
import PlayableRace from '../enum/PlayableRace'
import PlayableClass from '../enum/PlayableClass'
import PvPRank from '../enum/PvPRank'
import LockedItems from './LockedItems'
import LockedEnchants from './LockedEnchants'

export default interface OptionsCharacter {
  level: number
  gender: Gender
  race: PlayableRace
  class: PlayableClass
  pvpRank: PvPRank
  buffs: Array<string>
  lockedItems: LockedItems
  lockedEnchants: LockedEnchants
  talents: {
    naturesGraceRank: number
    moonFuryRank: number
    vengeanceRank: number
    improvedWrathRank: number
    improvedStarfireRank: number
    improvedMoonfireRank: number
    reflectionRank: number
    thickHideRank: number
    naturalWeaponsRank: number
    savageFuryRank: number
    predatoryStrikesRank: number
    heartOfTheWildRank: number
    primalFuryRank: number
    sharpenedClawsRank: number
    feralInstinctRank: number
    feralAgressionRank: number
    ferocityRank: number
    furorRank: number
    improvedEnrageRank: number
    leaderOfThePackRank: number
  }
}
