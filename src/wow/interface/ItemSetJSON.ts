import ItemJSON from '../interface/ItemJSON'

interface ItemSetBonus {
  pieces: number
  bonus: number
}

export default interface ItemSetJSON {
  name: string
  phase: number
  raid: boolean
  itemNames: string[]
  items?: ItemJSON[]
  score?: number
  mitScore?: number
  threatScore?: number
  spellHealing?: ItemSetBonus
  bonusArmor?: ItemSetBonus
  spellDamage: ItemSetBonus
  arcaneDamage: ItemSetBonus
  natureDamage: ItemSetBonus
  spellHit: ItemSetBonus
  spellCrit: ItemSetBonus
  spellPenetration: ItemSetBonus
  stamina: ItemSetBonus
  intellect: ItemSetBonus
  spirit: ItemSetBonus
  mp5: ItemSetBonus
  defense: ItemSetBonus
  strength: ItemSetBonus
  agility: ItemSetBonus
  haste: ItemSetBonus
  attackPower: ItemSetBonus
  meleeCrit: ItemSetBonus
  meleeHit: ItemSetBonus
  bonusHealth: ItemSetBonus
  dodge: ItemSetBonus
  armor: ItemSetBonus
  feralAttackPower: ItemSetBonus
}
