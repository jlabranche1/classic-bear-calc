import Options from '../interface/Options'
import Simulation from './Simulation'
import Equipment from './Equipment'

/*
 *  De facto top level object of the sim. Stores user set sim options
 *   and the simulation object that performs the sim calculations
 */
export default class Encounter {
  options: Options
  simulation: Simulation

  constructor(options: Options) {
    this.options = options
    const equipment = Equipment.optimalEquipment(this.options)

    this.simulation = new Simulation(options, equipment)
  }
}
