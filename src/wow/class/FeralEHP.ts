/// Damage mitigation from armor
/// https://classicwow.live/guides/46/basic-stats-sheet
export function damageMitigationFromArmor(armor: number, attackerLevel: number): number {
  const X = (0.1 * armor) / (8.5 * attackerLevel + 40)
  const mitigation = X / (1 + X)

  return Math.min(mitigation, 0.75)
}

export function armorCap(enemyLevel: number): number {
  // Damage mitigation formula from above but solve for armor
  // .75 = X / (1 + X)
  // x = 3
  // 3 = (.1 * armor) / (8.5 * enemyLevel + 40)
  return 3 * (8.5 * enemyLevel + 40) * 10
}

/// Includes crits but not dodges
export function effectiveHitPoints(
  health: number,
  armor: number,
  chanceToBeCriticallyHit: number,
  dodge: number,
  enemyLevel: number
) {
  return health / (1 - damageMitigationFromArmor(armor, enemyLevel)) / (1 + chanceToBeCriticallyHit - dodge)
}

export default { effectiveHitPoints, damageMitigationFromArmor }
