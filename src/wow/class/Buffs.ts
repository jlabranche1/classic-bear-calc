import Tools from '../module/Tools'

export enum Buff {
  None,
  MoonkinAura = 0,
  PowerInfusion = 1,
  EphemeralPower = 2,
  FlaskOfSupremePower = 3,
  GreaterArcaneElixir = 4,
  CerebralCortexCompound = 5,
  RunnTumTuberSurprise = 6,
  ArcaneBrilliance = 7,
  BlessingOfKings = 8,
  GiftOfTheWild = 9,
  RallyingCryOfTheDragonSlayer = 10,
  SlipkiksSavvy = 11,
  SongflowerSerenade = 12,
  SaygesDarkFortune = 13,
  TracesOfSilithyst = 14,
  BurningAdrenaline = 15,
  SpellVulnerability = 16,
  CurseOfShadow = 17,
  StormStrike = 18,
  SpiritOfZandalar = 19,
  SaygesDarkFortuneOfDamage = 20,
  WarchiefsBlessing = 21,
  FengusFerocity = 22,
  MoldarMoxie = 23,
  FlaskOfTitans = 24,
  SpiritOfZanza = 25,
  LungJuiceCocktail = 26,
  GroundScorpokAssay = 27,
  ROIDS = 28,
  MongooseElixir = 29,
  AgilityElixir = 30,
  JujuPower = 31,
  ElixirOfGiants = 32,
  JujuMight = 33,
  WinterfallFirewater = 34,
  ElixirOfFortitude = 35,
  GiftOfArthas = 36,
  DesertDumplings = 37,
  ChimaerokChops = 38,
  GrilledSquid = 39,
  RumseyRumBlackLabel = 40,
  GordokGreenGrog = 41,
  GreaterStoneshieldPotion = 42,
  BattleShout = 43,
  TrueshotAura = 44,
  PrayerOfFortitude = 45,
  Inspiration = 46,
  DevotionAura = 47,
  BlessingOfMight = 48,
  ImprovedLayOnHands = 49,
  ElixirOfSuperiorDefense = 50,
  GraceOfAirTotem = 51,
  StrengthOfEarthTotem = 52,
  TenderWolfSteak = 53,
  BloodPact = 54,
  SunderArmor = 55,
  ExposeArmor = 56,
  Annihilator = 57,
  HolySunder = 58,
  InsectSwarm = 59,
  FaerieFire = 60,
  CurseOfRecklessness = 61,
  DarkDesire = 62,
  ConsecratedSharpeningStone = 63
}
declare type BuffFlagType = keyof typeof Buff

enum BuffSlot {
  None = 0,
  Food = 1,
  BlastedLands = 2,
  StrengthBuff = 3,
  AttackPowerBuff = 4,
  Alcohol = 5,
  // Sunder/expose armor
  ArmorShred1 = 6,
  // Faerie fire
  ArmorShred2 = 7,
  Curse = 8,
  WeaponStone = 9
}

export interface BuffStatBonuses {
  armor: number
  strength: number
  agility: number
  stamina: number
  meleeHit: number
  meleeCrit: number
  attackPower: number
  dodge: number
  defense: number
  haste: number
  bonusArmor: number
  bonusHealth: number
  feralAttackPower: number
  intellect: number
  spirit: number
  spellCrit: number
  mp5: number
  spellDamage: number
  arcaneDamage: number
  natureDamage: number
  spellHit: number
}

const DEFAULT_STAT_BONUES = {
  armor: 0,
  strength: 0,
  agility: 0,
  stamina: 0,
  meleeHit: 0,
  meleeCrit: 0,
  attackPower: 0,
  dodge: 0,
  defense: 0,
  haste: 0,
  bonusArmor: 0,
  bonusHealth: 0,
  feralAttackPower: 0,
  intellect: 0,
  spirit: 0,
  spellCrit: 0,
  mp5: 0,
  spellDamage: 0,
  arcaneDamage: 0,
  natureDamage: 0,
  spellHit: 0
}

function isIterable(obj: any): boolean {
  // checks for null and undefined
  if (obj == null) {
    return false
  }
  return typeof obj[Symbol.iterator] === 'function'
}

interface BuffJSONInputs {
  name: string
  buffSlots: Iterable<BuffSlot> | BuffSlot
  buffStatBonuses: BuffStatBonuses
  tooltip: string
  // Rank of the spell of the buff if applicable
  rank?: number
  // Any other spells or set bonuses that might impact the power of the buff
  // (for display on buff tooltip)
  modifiers?: Array<string>
}

export class BuffJSON {
  tooltip?: string
  name: string
  buffSlots: Set<BuffSlot>
  buffStatBonuses: BuffStatBonuses
  rank?: number
  modifiers?: string[]

  constructor({ name, buffSlots, buffStatBonuses, tooltip, rank, modifiers }: BuffJSONInputs) {
    this.name = name
    this.buffSlots = isIterable(buffSlots) ? new Set(buffSlots as Iterable<BuffSlot>) : new Set([buffSlots as BuffSlot])
    this.buffSlots.delete(BuffSlot.None)
    this.buffStatBonuses = buffStatBonuses
    this.tooltip = tooltip
    this.rank = rank
    this.modifiers = modifiers
  }
}

// Valentine's Day Collections, Zanza Potions, and Blasted Lands consumables are
// all mutually exclusive. They don't stack, and consuming one of these types of items will
// overwrite the buff from one you may have taken previously.
const BUFFS = new Map([
  [
    Buff.FlaskOfTitans,
    new BuffJSON({
      name: 'Flask of Titans',
      buffSlots: BuffSlot.None,
      buffStatBonuses: { ...DEFAULT_STAT_BONUES, bonusHealth: 1200 },
      tooltip: 'Health increased by 1200.'
    })
  ],
  [
    Buff.BlessingOfKings,
    new BuffJSON({
      name: 'Blessing of Kings',
      buffSlots: BuffSlot.None,
      buffStatBonuses: { ...DEFAULT_STAT_BONUES },
      tooltip: 'Increases stats by 10%.'
    })
  ],
  [
    Buff.GiftOfTheWild,
    new BuffJSON({
      name: 'Gift of the Wild',
      buffSlots: BuffSlot.None,
      buffStatBonuses: {
        ...DEFAULT_STAT_BONUES,
        strength: 16,
        stamina: 16,
        agility: 16,
        intellect: 16,
        spirit: 16,
        bonusArmor: 385
      },
      tooltip: 'Increases armor by 385, all attributes by 16 and all resistances by 27.',
      rank: 2,
      modifiers: ['5/5 Improved Gift of the Wild']
    })
  ],
  [
    Buff.RallyingCryOfTheDragonSlayer,
    new BuffJSON({
      name: 'Rallying Cry of the Dragonslayer',
      buffSlots: BuffSlot.None,
      buffStatBonuses: { ...DEFAULT_STAT_BONUES, meleeCrit: 0.05, spellCrit: 10, attackPower: 140 },
      tooltip: 'Increases critical chance of spells by 10%, melee and ranged by 5% and grants 140 attack power.'
    })
  ],
  [
    Buff.SongflowerSerenade,
    new BuffJSON({
      name: 'Songflower Serenade',
      buffSlots: BuffSlot.None,
      buffStatBonuses: {
        ...DEFAULT_STAT_BONUES,
        meleeCrit: 0.05,
        strength: 15,
        stamina: 15,
        agility: 15,
        intellect: 15,
        spirit: 15
      },
      tooltip: 'Increases chance for a melee, ranged, or spell critical by 5% and all attributes by 15.'
    })
  ],
  [
    Buff.SpiritOfZandalar,
    new BuffJSON({
      name: 'Spirit of Zandalar',
      buffSlots: BuffSlot.None,
      buffStatBonuses: { ...DEFAULT_STAT_BONUES },
      tooltip: ' Increases movement speed by 10% and all stats by 15% for 2 hours.'
    })
  ],
  [
    Buff.SaygesDarkFortuneOfDamage,
    new BuffJSON({
      name: 'Sayges Dark Fortune of Damage',
      buffSlots: BuffSlot.None,
      buffStatBonuses: { ...DEFAULT_STAT_BONUES },
      tooltip: 'Increases damage by 10%.'
    })
  ],
  [
    Buff.WarchiefsBlessing,
    new BuffJSON({
      name: "Warchief's Blessing",
      buffSlots: BuffSlot.None,
      // NOTE: Haste is handled elsewhere due to it stacking multiplicitively
      buffStatBonuses: { ...DEFAULT_STAT_BONUES, bonusHealth: 300, mp5: 10 },
      tooltip: ' Increases hitpoints by 300. 15% haste to melee attacks. 10 mana regen every 5 seconds.'
    })
  ],
  [
    Buff.FengusFerocity,
    new BuffJSON({
      name: "Fengus' Ferocity",
      buffSlots: BuffSlot.None,
      buffStatBonuses: { ...DEFAULT_STAT_BONUES, attackPower: 200 },
      tooltip: 'Attack power increased by 200.'
    })
  ],
  [
    Buff.MoldarMoxie,
    new BuffJSON({
      name: "Mol'dar's Moxie",
      buffSlots: BuffSlot.None,
      buffStatBonuses: { ...DEFAULT_STAT_BONUES },
      tooltip: 'Overall Stamina increased by 15%.'
    })
  ],
  [
    Buff.SpiritOfZanza,
    new BuffJSON({
      name: 'Spirit of Zanza',
      buffSlots: BuffSlot.BlastedLands,
      buffStatBonuses: { ...DEFAULT_STAT_BONUES, stamina: 50, spirit: 50 },
      tooltip: 'Spirit increased by 50 and Stamina by 50.'
    })
  ],
  [
    Buff.LungJuiceCocktail,
    new BuffJSON({
      name: 'Lung Juice Cocktail',
      buffSlots: BuffSlot.BlastedLands,
      buffStatBonuses: { ...DEFAULT_STAT_BONUES, stamina: 25 },
      tooltip: 'Increases Stamina by 25.'
    })
  ],
  [
    Buff.GroundScorpokAssay,
    new BuffJSON({
      name: 'Ground Scorpok Assay',
      buffSlots: BuffSlot.BlastedLands,
      buffStatBonuses: { ...DEFAULT_STAT_BONUES, agility: 25 },
      tooltip: 'Increases Agility by 25.'
    })
  ],
  [
    Buff.ROIDS,
    new BuffJSON({
      name: 'R.O.I.D.S',
      buffSlots: BuffSlot.BlastedLands,
      buffStatBonuses: { ...DEFAULT_STAT_BONUES, strength: 25 },
      tooltip: 'Increases Strength by 25.'
    })
  ],
  [
    Buff.MongooseElixir,
    new BuffJSON({
      name: 'Elixir of the Mongoose',
      buffSlots: BuffSlot.None,
      buffStatBonuses: { ...DEFAULT_STAT_BONUES, agility: 25, meleeCrit: 0.02 },
      tooltip: 'Agility increased by 25, Critical hit chance increases by 2%. '
    })
  ],
  [
    Buff.JujuPower,
    new BuffJSON({
      name: 'Juju Power',
      buffSlots: BuffSlot.StrengthBuff,
      buffStatBonuses: { ...DEFAULT_STAT_BONUES, strength: 30 },
      tooltip: 'Increases Strength by 30.'
    })
  ],
  [
    Buff.ElixirOfGiants,
    new BuffJSON({
      name: 'Elixir of Giants',
      buffSlots: BuffSlot.StrengthBuff,
      buffStatBonuses: { ...DEFAULT_STAT_BONUES, strength: 25 },
      tooltip: 'Increases Strength by 25.'
    })
  ],
  [
    Buff.JujuMight,
    new BuffJSON({
      name: 'Juju Might',
      buffSlots: BuffSlot.AttackPowerBuff,
      buffStatBonuses: { ...DEFAULT_STAT_BONUES, attackPower: 40 },
      tooltip: 'Increases attack power by 40.'
    })
  ],
  [
    Buff.WinterfallFirewater,
    new BuffJSON({
      name: 'Winterfall Firewater',
      buffSlots: BuffSlot.AttackPowerBuff,
      buffStatBonuses: { ...DEFAULT_STAT_BONUES, attackPower: 35 },
      tooltip: 'Increases size, and melee attack power by 35.'
    })
  ],
  [
    Buff.ElixirOfFortitude,
    new BuffJSON({
      name: 'Elixir of Fortitude',
      buffSlots: BuffSlot.None,
      buffStatBonuses: { ...DEFAULT_STAT_BONUES, bonusHealth: 120 },
      tooltip: 'Health increased by 120.'
    })
  ],
  [
    Buff.DesertDumplings,
    new BuffJSON({
      name: 'Smoked Desert Dumpling',
      buffSlots: BuffSlot.Food,
      buffStatBonuses: { ...DEFAULT_STAT_BONUES, strength: 20 },
      tooltip: 'Increases Strength by 20.'
    })
  ],
  [
    Buff.ChimaerokChops,
    new BuffJSON({
      name: "Dirge's Kickin' Chimaerok Chops",
      buffSlots: BuffSlot.Food,
      buffStatBonuses: { ...DEFAULT_STAT_BONUES, stamina: 25 },
      tooltip: 'Increases Stamina by 25.'
    })
  ],
  [
    Buff.GrilledSquid,
    new BuffJSON({
      name: 'Grilled Squid',
      buffSlots: BuffSlot.Food,
      buffStatBonuses: { ...DEFAULT_STAT_BONUES, agility: 10 },
      tooltip: 'Increases Agility by 20.'
    })
  ],
  [
    Buff.RumseyRumBlackLabel,
    new BuffJSON({
      name: 'Rumsey Rum Black Label',
      buffSlots: BuffSlot.Alcohol,
      buffStatBonuses: { ...DEFAULT_STAT_BONUES, stamina: 15 },
      tooltip: 'Stamina increased by 15.'
    })
  ],
  [
    Buff.GordokGreenGrog,
    new BuffJSON({
      name: 'Gordok Green Grog',
      buffSlots: BuffSlot.Alcohol,
      buffStatBonuses: { ...DEFAULT_STAT_BONUES, stamina: 10 },
      tooltip: 'Stamina increased by 10.'
    })
  ],
  [
    Buff.GreaterStoneshieldPotion,
    new BuffJSON({
      name: 'Greater Stoneshield Potion',
      buffSlots: BuffSlot.None,
      buffStatBonuses: { ...DEFAULT_STAT_BONUES, bonusArmor: 2000 },
      tooltip: 'Increases armor by 2000. '
    })
  ],
  [
    Buff.BattleShout,
    new BuffJSON({
      name: 'Battle Shout',
      buffSlots: BuffSlot.None,
      buffStatBonuses: { ...DEFAULT_STAT_BONUES, attackPower: 290 },
      tooltip: 'Increases attack power by 290.',
      rank: 7,
      modifiers: ['5/5 Improved Battle Shout']
    })
  ],
  [
    Buff.PrayerOfFortitude,
    new BuffJSON({
      name: 'Prayer of Fortitude',
      buffSlots: BuffSlot.None,
      buffStatBonuses: { ...DEFAULT_STAT_BONUES, stamina: 70 },
      tooltip: 'Increases Stamina by 70.',
      rank: 2,
      modifiers: ['2/2 Improved Power Word: Fortitude']
    })
  ],
  [
    Buff.TrueshotAura,
    new BuffJSON({
      name: 'Trueshot Aura',
      buffSlots: BuffSlot.None,
      buffStatBonuses: { ...DEFAULT_STAT_BONUES, attackPower: 100 },
      tooltip: 'Increases attack power by 100.',
      rank: 3
    })
  ],
  [
    Buff.Inspiration,
    new BuffJSON({
      name: 'Inspiration',
      buffSlots: BuffSlot.None,
      buffStatBonuses: { ...DEFAULT_STAT_BONUES },
      tooltip: 'Increases armor by 25% for 15 secs.',
      rank: 3
    })
  ],
  [
    Buff.DevotionAura,
    new BuffJSON({
      name: 'Devotion Aura',
      buffSlots: BuffSlot.None,
      buffStatBonuses: { ...DEFAULT_STAT_BONUES, bonusArmor: 906 },
      tooltip: 'Increases armor by 906.',
      rank: 7,
      modifiers: ['5/5 Improved Devotion Aura']
    })
  ],
  [
    Buff.BlessingOfMight,
    new BuffJSON({
      name: 'Blessing of Might',
      buffSlots: BuffSlot.None,
      buffStatBonuses: { ...DEFAULT_STAT_BONUES, attackPower: 231 },
      tooltip: 'Increases attack power by 231.',
      rank: 7,
      modifiers: ['5/5 Improved Blessing of Might']
    })
  ],
  [
    Buff.ImprovedLayOnHands,
    new BuffJSON({
      name: 'Improved Lay on Hands',
      buffSlots: BuffSlot.None,
      buffStatBonuses: { ...DEFAULT_STAT_BONUES },
      tooltip: '30% bonus armor value from items.',
      rank: 2
    })
  ],
  [
    Buff.ElixirOfSuperiorDefense,
    new BuffJSON({
      name: 'Elixir of Superior Defense',
      buffSlots: BuffSlot.None,
      buffStatBonuses: { ...DEFAULT_STAT_BONUES, bonusArmor: 450 },
      tooltip: 'Armor increased by 450.'
    })
  ],
  [
    Buff.GraceOfAirTotem,
    new BuffJSON({
      name: 'Grace of Air Totem',
      buffSlots: BuffSlot.None,
      buffStatBonuses: { ...DEFAULT_STAT_BONUES, agility: 77 },
      tooltip: 'Agility increased by 77.',
      rank: 3
    })
  ],
  [
    Buff.StrengthOfEarthTotem,
    new BuffJSON({
      name: 'Strength of Earth Totem',
      buffSlots: BuffSlot.None,
      buffStatBonuses: { ...DEFAULT_STAT_BONUES, strength: 77 },
      tooltip: 'Strength increased by 77.',
      rank: 5
    })
  ],
  [
    Buff.TenderWolfSteak,
    new BuffJSON({
      name: 'Tender Wolf Steak',
      buffSlots: BuffSlot.Food,
      buffStatBonuses: { ...DEFAULT_STAT_BONUES, stamina: 12, spirit: 15 },
      tooltip: 'Stamina increased by 12 and Spirit increased by 15.'
    })
  ],
  [
    Buff.BloodPact,
    new BuffJSON({
      name: 'Blood Pact',
      buffSlots: BuffSlot.None,
      buffStatBonuses: { ...DEFAULT_STAT_BONUES, stamina: 42 },
      tooltip: 'Stamina increased by 42.',
      rank: 5
    })
  ],
  [
    Buff.SunderArmor,
    new BuffJSON({
      name: 'Sunder Armor',
      buffSlots: BuffSlot.ArmorShred1,
      buffStatBonuses: { ...DEFAULT_STAT_BONUES, armor: -2250 },
      tooltip: `Armor decreased by 2250.`,
      rank: 5,
      modifiers: ['5 Stacks']
    })
  ],
  [
    Buff.ExposeArmor,
    new BuffJSON({
      name: 'Improved Expose Armor',
      buffSlots: BuffSlot.ArmorShred1,
      buffStatBonuses: { ...DEFAULT_STAT_BONUES, armor: -2550 },
      tooltip: `Armor decreased by 2550.`,
      rank: 5,
      modifiers: ['2/2 Improved Expose Armor']
    })
  ],
  [
    Buff.Annihilator,
    new BuffJSON({
      name: 'Annihilator',
      buffSlots: BuffSlot.None,
      buffStatBonuses: { ...DEFAULT_STAT_BONUES, armor: -600 },
      tooltip: `Armor decreased by 600.`,
      modifiers: ['3 Stacks']
    })
  ],
  [
    Buff.HolySunder,
    new BuffJSON({
      name: 'HolySunder',
      buffSlots: BuffSlot.None,
      buffStatBonuses: { ...DEFAULT_STAT_BONUES, armor: -50 },
      tooltip: `Armor decreased by 50.`
    })
  ],
  [
    Buff.InsectSwarm,
    new BuffJSON({
      name: 'InsectSwarm',
      buffSlots: BuffSlot.None,
      buffStatBonuses: { ...DEFAULT_STAT_BONUES, meleeHit: -0.02 },
      tooltip: `Chance to hit decreased by 2% and 54 Nature damage every 2 sec.`,
      rank: 5
    })
  ],
  [
    Buff.FaerieFire,
    new BuffJSON({
      name: 'Faerie Fire',
      buffSlots: BuffSlot.ArmorShred2,
      buffStatBonuses: { ...DEFAULT_STAT_BONUES, armor: -505 },
      tooltip: `Decreases armor by 505.  Cannot stealth or turn invisible.`,
      rank: 4
    })
  ],
  [
    Buff.CurseOfRecklessness,
    new BuffJSON({
      name: 'Curse of Recklessness',
      buffSlots: BuffSlot.Curse,
      buffStatBonuses: { ...DEFAULT_STAT_BONUES, armor: -640, attackPower: 90 },
      tooltip: `Increases melee attack power by 90 and decreases armor by 640.   Target will not flee and will ignore Fear and Horror effects.`,
      rank: 4
    })
  ],
  [
    Buff.ConsecratedSharpeningStone,
    new BuffJSON({
      name: 'Consecrated Sharpening Stone',
      buffSlots: BuffSlot.WeaponStone,
      buffStatBonuses: { ...DEFAULT_STAT_BONUES, attackPower: 100 },
      tooltip: `While applied to target weapon it increases attack power against undead by 100. Lasts for 1 hour.`,
      modifiers: ['NOTE: Undead target type check is ignored in the sim']
    })
  ],
  [
    Buff.DarkDesire,
    new BuffJSON({
      name: 'Dark Desire',
      buffSlots: BuffSlot.None,
      buffStatBonuses: { ...DEFAULT_STAT_BONUES, meleeHit: 0.02 },
      tooltip: `Improves your chance to hit by 2% for 1 hour.`
    })
  ]
])

// Given a new list of buffs with at most one new buff and the old buff list will
// return the list of buffs your player would recieve by disabling
// any old buffs that get overwritten or do not stack with the new buff
export function enableBuff(newBuffList: string[], currentBuffs: string[]): string[] {
  // If we are strictly removing buffs simply return the new list
  if (newBuffList.length < currentBuffs.length) {
    return newBuffList
  }
  // Parse out the newly added buff from the list of new buffs
  const newBuffs = Tools.difference(new Set(newBuffList), new Set(currentBuffs))
  console.assert(newBuffs.size === 1, `Expected one new buff got ${newBuffs}`)
  const newBuff = newBuffs.values().next().value
  const newBuffJSON = BUFFS.get(Buff[newBuff as BuffFlagType]) as BuffJSON

  console.debug(`Enabling buff ${newBuffJSON.name}`)
  console.assert((newBuffJSON as undefined | BuffJSON) !== undefined, `Could not find buff ${newBuff}`)

  // Determine which buffs from the current buffs conflict with
  // the new buff
  const newBuffSlots = newBuffJSON.buffSlots
  const currentBuffSet = new Set(currentBuffs)
  const buffsToDelete: Set<string> = new Set()
  for (const buff of currentBuffSet) {
    const b = (BUFFS.get(Buff[buff as BuffFlagType]) as BuffJSON).buffSlots
    if (Tools.intersection(b, newBuffSlots).size) {
      buffsToDelete.add(buff)
    }
  }

  // Remove buffs that conflict with the new buff
  console.debug(`Disabling buffs ${Array.from(buffsToDelete)}`)
  for (const buffToDelete of buffsToDelete) {
    currentBuffSet.delete(buffToDelete)
  }
  currentBuffSet.add(newBuff)
  return Array.from(currentBuffSet)
}

type BuffStatKey = keyof BuffStatBonuses

function sumObjectsByKey(objs: BuffStatBonuses[]) {
  return objs.reduce(
    (a, b) => {
      for (const k in b) {
        if (b.hasOwnProperty(k)) a[k as BuffStatKey] = (a[k as BuffStatKey] || 0) + b[k as BuffStatKey]
      }
      return a
    },
    { ...DEFAULT_STAT_BONUES }
  )
}

export function sumBuffStatBonuses(buffs: string[]): BuffStatBonuses {
  const buffJsons: BuffJSON[] = []
  for (const buff of buffs.map((b) => Buff[b as BuffFlagType])) {
    const b = BUFFS.get(buff)
    console.assert(b !== undefined, `Could not find buff ${b}`)
    buffJsons.push(b as BuffJSON)
  }
  const summedBuffBonuses = sumObjectsByKey(buffJsons.map((b) => b.buffStatBonuses))

  console.debug(`Stat Bonuses from Buffs ${JSON.stringify(summedBuffBonuses)}`)
  return summedBuffBonuses
}

export function getBuffJSONByName(buff: string): BuffJSON {
  return BUFFS.get(Buff[buff as BuffFlagType]) as BuffJSON
}

export function inspirationArmorModifer() {
  return 1.25
}

export function improvedLayOnHandsArmorModifier() {
  return 1.3
}

export function blessingOfKingsStatModifier() {
  return 1.1
}

export function spiritOfZandalarStatModifier() {
  return 1.15
}

export function moldarsMoxieStamModifier() {
  return 1.15
}

export default {
  enableBuff,
  Buff,
  BuffJSON,
  sumBuffStatBonuses,
  inspirationArmorModifer,
  improvedLayOnHandsArmorModifier,
  blessingOfKingsStatModifier,
  spiritOfZandalarStatModifier,
  moldarsMoxieStamModifier,
  getBuffJSONByName
}
