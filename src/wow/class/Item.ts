import Query from '../module/Query'

import ItemJSON from '../interface/ItemJSON'
import EnchantJSON from '../interface/EnchantJSON'

import ItemQuality from '../enum/ItemQuality'
import ItemSlot from '../enum/ItemSlot'
import ItemClass from '../enum/ItemClass'
import ArmorSubclass from '../enum/ArmorSubclass'
import WeaponSubclass from '../enum/WeaponSubclass'
import PlayableClass from '../enum/PlayableClass'
import PvPRank from '../enum/PvPRank'
import Faction from '../enum/Faction'
import TargetType from '../enum/TargetType'
import Weights from '../interface/Weights'

interface SetBonusInfo {
  setName: string
  activeSetBonusValues: Array<string>
  inactiveSetBonusValues: Array<string>
  setItemsEquipped: Set<string>
  setItemsUnequipped: Set<string>
}

export default class Item {
  _slot: ItemSlot
  itemJSON: ItemJSON | undefined
  enchantJSON: EnchantJSON | undefined
  setBonusInfo: SetBonusInfo

  constructor(slot: ItemSlot, itemJSON?: ItemJSON, enchantJSON?: EnchantJSON) {
    this._slot = slot
    this.itemJSON = itemJSON ? itemJSON : undefined
    this.enchantJSON = enchantJSON ? enchantJSON : undefined
    this.setBonusInfo = {
      setName: '',
      activeSetBonusValues: [],
      inactiveSetBonusValues: [],
      setItemsEquipped: new Set(),
      setItemsUnequipped: new Set()
    }
  }

  static sortScoreAsc(a: ItemJSON | EnchantJSON, b: ItemJSON | EnchantJSON) {
    return (a.score ? a.score : 0) - (b.score ? b.score : 0)
  }

  static sortScoreDes(a: ItemJSON | EnchantJSON, b: ItemJSON | EnchantJSON) {
    return (b.score ? b.score : 0) - (a.score ? a.score : 0)
  }

  static scoreItem(item: ItemJSON, ehpWeights: Weights, tpsWeights: Weights): number {
    return parseFloat(this.score(ehpWeights, tpsWeights, item).toFixed(3))
  }

  static scoreEnchant(enchant: EnchantJSON, ehpWeights: Weights, tpsWeights: Weights): number {
    return parseFloat(this.score(ehpWeights, tpsWeights, enchant).toFixed(3))
  }

  static score(ehpWeights: Weights, tpsWeights: Weights, item: ItemJSON | EnchantJSON): number {
    return Item.threatScore(tpsWeights, item) + Item.mitScore(ehpWeights, item)
  }

  static mitScore(ehpWeights: Weights, item: ItemJSON | EnchantJSON | undefined): number {
    if (item === undefined) {
      return 0
    }
    const ehpScore =
      (item.armor ? item.armor * ehpWeights.armor : 0) +
      (item.strength ? item.strength * ehpWeights.strength : 0) +
      (item.stamina ? item.stamina * ehpWeights.stamina : 0) +
      (item.agility ? item.agility * ehpWeights.agility : 0) +
      (item.meleeCrit ? item.meleeCrit * ehpWeights.meleeCrit : 0) +
      (item.meleeHit ? item.meleeHit * ehpWeights.meleeHit : 0) +
      (item.attackPower ? item.attackPower * ehpWeights.attackPower : 0) +
      (item.feralAttackPower ? item.feralAttackPower * ehpWeights.feralAttackPower : 0) +
      (item.defense ? item.defense * ehpWeights.defense : 0) +
      (item.dodge ? item.dodge * ehpWeights.dodge : 0) +
      (item.haste ? item.haste * ehpWeights.haste : 0) +
      (item.bonusArmor ? item.bonusArmor * ehpWeights.bonusArmor : 0) +
      (item.bonusHealth ? item.bonusHealth * ehpWeights.bonusHealth : 0) +
      (item.onUseMitScore !== undefined ? item.onUseMitScore : 0)
    return parseFloat(ehpScore.toFixed(3))
  }

  static threatScore(tpsWeights: Weights, item: ItemJSON | EnchantJSON | undefined): number {
    if (item === undefined) {
      return 0
    }
    const tpsScore =
      (item.armor ? item.armor * tpsWeights.armor : 0) +
      (item.strength ? item.strength * tpsWeights.strength : 0) +
      (item.stamina ? item.stamina * tpsWeights.stamina : 0) +
      (item.agility ? item.agility * tpsWeights.agility : 0) +
      (item.meleeCrit ? item.meleeCrit * tpsWeights.meleeCrit : 0) +
      (item.meleeHit ? item.meleeHit * tpsWeights.meleeHit : 0) +
      (item.attackPower ? item.attackPower * tpsWeights.attackPower : 0) +
      (item.feralAttackPower ? item.feralAttackPower * tpsWeights.feralAttackPower : 0) +
      (item.defense ? item.defense * tpsWeights.defense : 0) +
      (item.dodge ? item.dodge * tpsWeights.dodge : 0) +
      (item.haste ? item.haste * tpsWeights.haste : 0) +
      (item.bonusArmor ? item.bonusArmor * tpsWeights.bonusArmor : 0) +
      (item.bonusHealth ? item.bonusHealth * tpsWeights.bonusHealth : 0) +
      (item.onUseThreatScore !== undefined ? item.onUseThreatScore : 0)
    return parseFloat(tpsScore.toFixed(3))
  }

  get id(): number {
    return this.itemJSON && this.itemJSON.id ? this.itemJSON.id : 0
  }

  get customId(): string {
    return this.itemJSON && this.itemJSON.customId ? this.itemJSON.customId : ``
  }

  get enchantId(): number {
    return this.enchantJSON && this.enchantJSON.id ? this.enchantJSON.id : 0
  }

  get enchantCustomId(): string {
    return this.enchantJSON && this.enchantJSON.customId ? this.enchantJSON.customId : ``
  }

  get name(): string {
    return this.itemJSON && this.itemJSON.name ? this.itemJSON.name : this.slotDisplayName
  }

  get class(): ItemClass {
    if (this.itemJSON && this.itemJSON.class) {
      return this.itemJSON.class
    }

    switch (this.slot) {
      case ItemSlot.Twohand:
      case ItemSlot.Mainhand:
      case ItemSlot.Onehand:
        return ItemClass.Weapon
      default:
        return ItemClass.Armor
    }
  }

  get isWeapon(): boolean {
    return this.class === ItemClass.Weapon
  }

  get isArmor(): boolean {
    return this.class === ItemClass.Armor
  }

  get isTwoHander(): boolean {
    return (
      this.isWeapon &&
      [
        WeaponSubclass.TwoHandedMace,
        WeaponSubclass.TwoHandedExotic,
        WeaponSubclass.TwoHandedAxe,
        WeaponSubclass.TwoHandedSword,
        WeaponSubclass.Staff,
        WeaponSubclass.Spear,
        WeaponSubclass.FishingPole
      ].includes(this.subclass as WeaponSubclass)
    )
  }

  get subclass(): WeaponSubclass | ArmorSubclass {
    if (this.itemJSON && this.itemJSON.subclass) {
      return this.itemJSON.subclass
    }

    if (this.class === ItemClass.Weapon) {
      return WeaponSubclass.Empty
    }

    return ArmorSubclass.Empty
  }

  get subclassName(): string {
    if (this.class === ItemClass.Armor) {
      switch (this.subclass) {
        default:
          return ArmorSubclass[this.subclass]
      }
    }

    switch (this.subclass) {
      case WeaponSubclass.OneHandedMace:
        return 'Mace'
      default:
        return WeaponSubclass[this.subclass]
    }
  }

  get slot(): ItemSlot {
    return this.itemJSON ? this.itemJSON.slot : this._slot
  }

  get slotName(): string {
    switch (this.slot) {
      case ItemSlot.Trinket2:
        return ItemSlot[ItemSlot.Trinket]
      case ItemSlot.Finger2:
        return ItemSlot[ItemSlot.Finger]
      case ItemSlot.Mainhand:
        return 'Main Hand'
      case ItemSlot.Ammo:
      case ItemSlot.Head:
      case ItemSlot.Neck:
      case ItemSlot.Shoulder:
      case ItemSlot.Shirt:
      case ItemSlot.Chest:
      case ItemSlot.Waist:
      case ItemSlot.Legs:
      case ItemSlot.Feet:
      case ItemSlot.Wrist:
      case ItemSlot.Hands:
      case ItemSlot.Finger:
      case ItemSlot.Trinket:
      case ItemSlot.Onehand:
      case ItemSlot.Ranged:
      case ItemSlot.Back:
      case ItemSlot.Twohand:
      case ItemSlot.Tabard:
      case ItemSlot.Offhand:
      case ItemSlot.Projectile:
      case ItemSlot.Relic:
      default:
        return ItemSlot[this.slot]
    }
  }

  get slotDisplayName(): string {
    switch (this.slot) {
      case ItemSlot.Onehand:
      case ItemSlot.Twohand:
      case ItemSlot.Mainhand:
        return 'Main Hand'
      case ItemSlot.Finger:
        return 'Finger 1'
      case ItemSlot.Finger2:
        return 'Finger 2'
      case ItemSlot.Offhand:
        return 'Off Hand'
      case ItemSlot.Trinket:
        return 'Trinket 1'
      case ItemSlot.Trinket2:
        return 'Trinket 2'
      case ItemSlot.Ammo:
      case ItemSlot.Head:
      case ItemSlot.Neck:
      case ItemSlot.Shoulder:
      case ItemSlot.Shirt:
      case ItemSlot.Chest:
      case ItemSlot.Waist:
      case ItemSlot.Legs:
      case ItemSlot.Feet:
      case ItemSlot.Wrist:
      case ItemSlot.Hands:
      case ItemSlot.Ranged:
      case ItemSlot.Back:
      case ItemSlot.Tabard:
      case ItemSlot.Projectile:
      case ItemSlot.Relic:
      default:
        return ItemSlot[this.slot]
    }
  }

  get isEmpty(): boolean {
    if (!this.itemJSON) {
      return true
    }

    if (this.itemJSON.customId === '1') {
      return true
    }

    return false
  }

  get quality(): ItemQuality {
    return this.itemJSON && this.itemJSON.quality ? this.itemJSON.quality : ItemQuality.Poor
  }

  get qualityName(): string {
    return ItemQuality[this.quality]
  }

  get level(): number {
    return this.itemJSON && this.itemJSON.level ? this.itemJSON.level : 0
  }

  get reqLevel(): number {
    return this.itemJSON && this.itemJSON.reqLevel ? this.itemJSON.reqLevel : 0
  }

  get isBop(): boolean {
    return this.itemJSON && this.itemJSON.bop ? true : false
  }

  get isUnique(): boolean {
    return this.itemJSON && this.itemJSON.unique ? this.itemJSON.unique : false
  }

  get allowableClasses(): PlayableClass[] {
    return this.itemJSON && this.itemJSON.allowableClasses ? this.itemJSON.allowableClasses : []
  }

  get allowableClassesText(): string {
    const ac = this.allowableClasses
    let text = ''

    for (let _i = 0; _i < ac.length; _i++) {
      text += PlayableClass[ac[_i]]
      if (_i < ac.length - 1) {
        text += ', '
      }
    }
    return text
  }

  get targetTypes(): TargetType {
    return this.itemJSON && this.itemJSON.targetTypes ? this.itemJSON.targetTypes : TargetType.All
  }

  get phase(): number {
    return this.itemJSON && this.itemJSON.phase ? this.itemJSON.phase : 1
  }

  get pvpRank(): PvPRank {
    return this.itemJSON && this.itemJSON.pvpRank ? this.itemJSON.pvpRank : PvPRank.Grunt
  }

  get icon(): string {
    let emptySlot = `${this.slotName.split(' ').join('')}`
    if (emptySlot === 'Offhand') {
      emptySlot = 'OffHand'
    }
    if (emptySlot === 'Onehand') {
      emptySlot = 'MainHand'
    }

    return this.isEmpty || !this.itemJSON ? `${emptySlot}.jpg` : `${this.itemJSON.icon}.jpg`
  }

  get iconFullPath(): string {
    return process.env.BASE_URL + 'wow-icons/' + this.icon
  }

  get location(): string {
    return this.itemJSON && this.itemJSON.location ? this.itemJSON.location : ''
  }

  get boss(): string {
    return this.itemJSON && this.itemJSON.boss ? this.itemJSON.boss : ''
  }

  get worldBoss(): boolean {
    return this.itemJSON && this.itemJSON.worldBoss === true ? true : false
  }

  get faction(): Faction {
    return this.itemJSON && this.itemJSON.faction ? this.itemJSON.faction : Faction.Horde
  }

  get score(): number {
    const mitScore = this.itemJSON && this.itemJSON.mitScore ? this.itemJSON.mitScore : 0
    const threatScore = this.itemJSON && this.itemJSON.threatScore ? this.itemJSON.threatScore : 0
    return mitScore + threatScore
  }

  get mitScore(): number {
    return this.itemJSON && this.itemJSON.mitScore ? this.itemJSON.mitScore : 0
  }

  get threatScore(): number {
    return this.itemJSON && this.itemJSON.threatScore ? this.itemJSON.threatScore : 0
  }

  get onUseText(): string {
    if (!this.itemJSON || !this.itemJSON.onUse || !this.itemJSON.onUse.effect) {
      return ``
    }

    const effect = this.itemJSON.onUse.effect
    const cooldown = this.itemJSON.onUse.cooldown ? `(${this.itemJSON.onUse.cooldown})` : ``

    return `${effect} ${cooldown}`
  }

  get hasOnUse(): boolean {
    return this.itemJSON && this.itemJSON.onUse ? true : false
  }

  get bindText(): string {
    return 'Binds ' + (this.isBop ? 'when picked up' : 'when equipped')
  }

  get _stamina(): number {
    return this.itemJSON && this.itemJSON.stamina ? this.itemJSON.stamina : 0
  }

  get stamina(): number {
    return this._stamina + (this.enchantJSON && this.enchantJSON.stamina ? this.enchantJSON.stamina : 0)
  }

  get _spirit(): number {
    return this.itemJSON && this.itemJSON.spirit ? this.itemJSON.spirit : 0
  }

  get spirit(): number {
    return this._spirit + (this.enchantJSON && this.enchantJSON.spirit ? this.enchantJSON.spirit : 0)
  }

  get _spellHealing(): number {
    return this.itemJSON && this.itemJSON.spellHealing ? this.itemJSON.spellHealing : 0
  }

  get spellHealing(): number {
    return this._spellHealing + (this.enchantJSON && this.enchantJSON.spellHealing ? this.enchantJSON.spellHealing : 0)
  }

  get _spellDamage(): number {
    return this.itemJSON && this.itemJSON.spellDamage ? this.itemJSON.spellDamage : 0
  }

  get spellDamage(): number {
    return this._spellDamage + (this.enchantJSON && this.enchantJSON.spellDamage ? this.enchantJSON.spellDamage : 0)
  }

  get spellPenetration(): number {
    return this.itemJSON && this.itemJSON.spellPenetration ? this.itemJSON.spellPenetration : 0
  }

  get _arcaneDamage(): number {
    return this.itemJSON && this.itemJSON.arcaneDamage ? this.itemJSON.arcaneDamage : 0
  }

  get arcaneDamage(): number {
    return this._arcaneDamage + (this.enchantJSON ? this.enchantJSON.arcaneDamage : 0)
  }

  get _natureDamage(): number {
    return this.itemJSON && this.itemJSON.natureDamage ? this.itemJSON.natureDamage : 0
  }

  get natureDamage(): number {
    return this._natureDamage + (this.enchantJSON ? this.enchantJSON.natureDamage : 0)
  }

  get _spellHit(): number {
    return this.itemJSON && this.itemJSON.spellHit ? this.itemJSON.spellHit : 0
  }

  get spellHit(): number {
    return this._spellHit + (this.enchantJSON ? this.enchantJSON.spellHit : 0)
  }

  get _spellCrit(): number {
    return this.itemJSON && this.itemJSON.spellCrit ? this.itemJSON.spellCrit : 0
  }

  get spellCrit(): number {
    return this._spellCrit + (this.enchantJSON ? this.enchantJSON.spellCrit : 0)
  }

  get _intellect(): number {
    return this.itemJSON && this.itemJSON.intellect ? this.itemJSON.intellect : 0
  }

  get intellect(): number {
    return this._intellect + (this.enchantJSON && this.enchantJSON.intellect ? this.enchantJSON.intellect : 0)
  }

  get _mp5(): number {
    return this.itemJSON && this.itemJSON.mp5 ? this.itemJSON.mp5 : 0
  }

  get mp5(): number {
    return this._mp5 + (this.enchantJSON && this.enchantJSON.mp5 ? this.enchantJSON.mp5 : 0)
  }

  get _armor(): number {
    return this.itemJSON && this.itemJSON.armor ? this.itemJSON.armor : 0
  }
  get _bonusArmor(): number {
    return this.itemJSON && this.itemJSON.bonusArmor ? this.itemJSON.bonusArmor : 0
  }
  get _bonusHealth(): number {
    return this.itemJSON && this.itemJSON.bonusHealth ? this.itemJSON.bonusHealth : 0
  }

  get armor(): number {
    return this._armor
  }

  get bonusArmor(): number {
    return this._bonusArmor + (this.enchantJSON && this.enchantJSON.bonusArmor ? this.enchantJSON.bonusArmor : 0)
  }

  get bonusHealth(): number {
    return this._bonusHealth + (this.enchantJSON && this.enchantJSON.bonusHealth ? this.enchantJSON.bonusHealth : 0)
  }

  get defense(): number {
    return this._defense + (this.enchantJSON && this.enchantJSON.defense ? this.enchantJSON.defense : 0)
  }

  get _defense(): number {
    return this.itemJSON && this.itemJSON.defense ? this.itemJSON.defense : 0
  }

  get _dodge(): number {
    return this.itemJSON && this.itemJSON.dodge ? this.itemJSON.dodge : 0
  }

  get dodge(): number {
    return this._dodge + (this.enchantJSON && this.enchantJSON.dodge ? this.enchantJSON.dodge : 0)
  }

  get durability(): number {
    return this.itemJSON && this.itemJSON.durability ? this.itemJSON.durability : 0
  }

  get minDmg(): number {
    return this.itemJSON && this.itemJSON.minDmg ? this.itemJSON.minDmg : 0
  }

  get maxDmg(): number {
    return this.itemJSON && this.itemJSON.maxDmg ? this.itemJSON.maxDmg : 0
  }

  get dmgText(): string {
    const minDmg = this.itemJSON && this.itemJSON.minDmg ? this.itemJSON.minDmg.toFixed(0) : 0
    const maxDmg = this.itemJSON && this.itemJSON.maxDmg ? this.itemJSON.maxDmg.toFixed(0) : 0
    return `${minDmg} - ${maxDmg}`
  }

  get speed(): number {
    return this.itemJSON && this.itemJSON.speed ? this.itemJSON.speed : 0
  }

  get speedText(): string {
    if (!this.itemJSON || !this.itemJSON.speed) {
      return ''
    }
    return `${parseFloat(this.itemJSON.speed.toFixed(1)).toFixed(2)}`
  }

  get dps(): number {
    return this.itemJSON && this.itemJSON.dps ? this.itemJSON.dps : 0
  }

  get dpsText(): string {
    if (!this.itemJSON || !this.itemJSON.dps) {
      return ''
    }
    return `${parseFloat(this.itemJSON.dps.toFixed(1)).toFixed(2)}`
  }

  get _strength(): number {
    return this.itemJSON && this.itemJSON.strength ? this.itemJSON.strength : 0
  }

  get strength(): number {
    return this._strength + (this.enchantJSON && this.enchantJSON.strength ? this.enchantJSON.strength : 0)
  }

  get _agility(): number {
    return this.itemJSON && this.itemJSON.agility ? this.itemJSON.agility : 0
  }

  get agility(): number {
    return this._agility + (this.enchantJSON && this.enchantJSON.agility ? this.enchantJSON.agility : 0)
  }

  get _attackPower(): number {
    return this.itemJSON && this.itemJSON.attackPower ? this.itemJSON.attackPower : 0
  }

  get attackPower(): number {
    return this._attackPower + (this.enchantJSON && this.enchantJSON.attackPower ? this.enchantJSON.attackPower : 0)
  }

  get _feralAttackPower(): number {
    return this.itemJSON && this.itemJSON.feralAttackPower ? this.itemJSON.feralAttackPower : 0
  }

  get feralAttackPower(): number {
    return this._feralAttackPower
  }

  get _meleeHit(): number {
    return this.itemJSON && this.itemJSON.meleeHit ? this.itemJSON.meleeHit : 0
  }

  get meleeHit(): number {
    return this._meleeHit
  }

  get _haste(): number {
    return this.itemJSON && this.itemJSON.haste ? this.itemJSON.haste / 100 + 1 : 1
  }

  get haste(): number {
    return this._haste * (this.enchantJSON && this.enchantJSON.haste ? this.enchantJSON.haste / 100 + 1 : 1)
  }

  get _meleeCrit(): number {
    return this.itemJSON && this.itemJSON.meleeCrit ? this.itemJSON.meleeCrit : 0
  }

  get meleeCrit(): number {
    return this._meleeCrit + (this.enchantJSON && this.enchantJSON.meleeCrit ? this.enchantJSON.meleeCrit : 0)
  }

  get enchantText(): string {
    const slot = this.itemJSON ? this.itemJSON.slot : ItemSlot.Any
    const text = this.enchantJSON ? this.enchantJSON.text : 'No Enchant'

    switch (slot) {
      case ItemSlot.Head:
      case ItemSlot.Hands:
      case ItemSlot.Shoulder:
      case ItemSlot.Legs:
      case ItemSlot.Back:
      case ItemSlot.Feet:
      case ItemSlot.Chest:
      case ItemSlot.Wrist:
      case ItemSlot.Twohand:
      case ItemSlot.Onehand:
      case ItemSlot.Mainhand:
        return text
      default:
        return ``
    }
  }

  get enchantClass(): string {
    const slot = this.enchantJSON ? this.enchantJSON.slot : ItemSlot.Any

    if (this.enchantJSON && this.enchantJSON.id === 1) {
      return `poor`
    }

    switch (slot) {
      case ItemSlot.Head:
      case ItemSlot.Hands:
      case ItemSlot.Shoulder:
      case ItemSlot.Legs:
      case ItemSlot.Back:
      case ItemSlot.Feet:
      case ItemSlot.Chest:
      case ItemSlot.Wrist:
      case ItemSlot.Mainhand:
        return `uncommon`
      case ItemSlot.Any:
      default:
        return `poor`
    }
  }

  static formatStatBonus(stat: string, amount: number, isSetBonusTooltip: boolean = false): string {
    const prefix = isSetBonusTooltip ? 'Set:' : 'Equip:'
    switch (stat) {
      case 'meleeHit':
        return `${prefix} Improves your chance to hit by ${amount}%.`
      case 'haste':
        if ((amount * 100) > 100) {
          return `${prefix} Increases your attack and casting speed by ${((amount - 1) * 100).toFixed()}%.`
        }
        return ``
      case 'meleeCrit':
        return `${prefix} Improves your chance to get a critical strike by ${amount}%.`
      case 'attackPower':
        return `+${amount} Attack Power.`
      case 'feralAttackPower':
        return `+${amount} Attack Power in Cat, Bear, and Dire Bear forms only.`
      case 'bonusArmor':
        return `${prefix} +${amount} Armor.`
      case 'spellHit':
        return `${prefix} Improves your chance to hit with spells by ${amount}%.`
      case 'spellCrit':
        return `${prefix} Improves your chance to get a critical strike with spells by ${amount}%.`
      case 'dodge':
        return `${prefix} Increases your chance to dodge an attack by ${amount}%.`
      case 'defense':
        return `${prefix} Increased Defense +${amount}.`
      case 'spellDamage':
        return `${prefix} Increases damage and healing done by magical spells and effects by up to ${amount}.`
      case 'arcaneDamage':
        return `${prefix} Increases damage done by Arcane spells and effects by up to ${amount}.`
      case 'natureDamage':
        return `${prefix} Increases damage done by Nature spells and effects by up to ${amount}.`
      case 'mp5':
        return `${prefix} Restores ${amount} mana per 5 sec.`
      case 'strength':
        return `+${amount} Strength`
      case 'agility':
        return `+${amount} Agility`
      case 'intellect':
        return `+${amount} Intellect`
      case 'stamina':
        return `+${amount} Stamina`
      case 'spirit':
        return `+${amount} Spirit`
      default:
        // Pass through for custom stats like "Equip" effects or non-standar set bonuses
        return `${prefix} ${amount}`
    }
  }

  get bonusesList(): string[] {
    const bonuses: string[] = []
    if (
      this.name.includes(`of Arcane Wrath`) ||
      this.name.includes(`of Nature's Wrath`) ||
      this.name.includes(`of Sorcery`) ||
      this.name.includes(`of Restoration`)
    ) {
      if (this._spellHit > 0) {
        bonuses.push(Item.formatStatBonus('spellHit', this._spellHit))
      }
      if (this._spellCrit > 0) {
        bonuses.push(Item.formatStatBonus('spellCrit', this._spellCrit))
      }
      return bonuses
    }

    if (this._meleeHit > 0) {
      bonuses.push(Item.formatStatBonus('meleeHit', this._meleeHit))
    }

    if (this._haste > 0) {
      bonuses.push(Item.formatStatBonus('haste', this._haste))
    }

    if (this._meleeCrit > 0) {
      bonuses.push(Item.formatStatBonus('meleeCrit', this._meleeCrit))
    }

    if (this._attackPower > 0) {
      bonuses.push(Item.formatStatBonus('attackPower', this._attackPower))
    }

    if (this._feralAttackPower > 0) {
      bonuses.push(Item.formatStatBonus('feralAttackPower', this._feralAttackPower))
    }
    if (this._spellHit > 0) {
      bonuses.push(Item.formatStatBonus('spellHit', this._spellHit))
    }

    if (this._spellCrit > 0) {
      bonuses.push(Item.formatStatBonus('spellCrit', this._spellCrit))
    }
    if (this._dodge > 0) {
      bonuses.push(Item.formatStatBonus('dodge', this._dodge))
    }
    if (this._defense > 0) {
      bonuses.push(Item.formatStatBonus('defense', this._defense))
    }

    if (this._spellDamage > 0) {
      bonuses.push(Item.formatStatBonus('spellDamage', this._spellDamage))
    }

    if (this._arcaneDamage > 0) {
      bonuses.push(Item.formatStatBonus('arcaneDamge', this._arcaneDamage))
    }

    if (this._natureDamage > 0) {
      bonuses.push(Item.formatStatBonus('natureDamage', this._natureDamage))
    }

    if (this._mp5 > 0) {
      bonuses.push(Item.formatStatBonus('mp5', this._mp5))
    }

    return bonuses
  }

  get statsList(): Object[] {
    const stats: Object[] = []
    if (this.name.includes(`of Arcane Wrath`)) {
      stats.push({ stat: 'Arcane Damage', value: this._arcaneDamage, type: 'primary' })
    } else if (this.name.includes(`of Nature's Wrath`)) {
      stats.push({ stat: 'Nature Damage', value: this._natureDamage, type: 'primary' })
    } else if (this.name.includes(`of Sorcery`)) {
      stats.push({ stat: 'Intellect', value: this._intellect, type: 'primary' })
      stats.push({ stat: 'Stamina', value: this._stamina, type: 'primary' })
      stats.push({ stat: 'Damage and Healing Spells', value: this._spellDamage, type: 'primary' })
    } else if (this.name.includes(`of Restoration`)) {
      stats.push({ stat: 'Stamina', value: this._stamina, type: 'primary' })
      stats.push({ stat: 'Healing Spells', value: this._spellHealing, type: 'primary' })
      stats.push({ stat: 'mana every 5 sec', value: this._mp5, type: 'primary' })
    } else {
      if (this._strength > 0) {
        stats.push({ stat: 'Strength', value: this._strength, type: 'primary' })
      }
      if (this._agility > 0) {
        stats.push({ stat: 'Agility', value: this._agility, type: 'primary' })
      }
      if (this._intellect > 0) {
        stats.push({ stat: 'Intellect', value: this._intellect, type: 'primary' })
      }

      if (this._stamina > 0) {
        stats.push({ stat: 'Stamina', value: this._stamina, type: 'primary' })
      }

      if (this._spirit > 0) {
        stats.push({ stat: 'Spirit', value: this._spirit, type: 'primary' })
      }
    }

    return stats
  }

  get chanceOnHitList(): string[] {
    const arr: string[] = []

    return arr
  }

  toJSON() {
    const proto = Object.getPrototypeOf(this)
    const jsonObj: any = Object.assign({}, this)

    Object.entries(Object.getOwnPropertyDescriptors(proto))
      .filter(([key, descriptor]) => typeof descriptor.get === 'function')
      .map(([key, descriptor]) => {
        if (descriptor && key[0] !== '_') {
          try {
            const val = (this as any)[key]
            jsonObj[key] = val
          } catch (error) {
            console.error(`Error calling getter ${key}`, error)
          }
        }
      })

    return jsonObj
  }
}
