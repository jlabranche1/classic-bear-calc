import Character from './Character'
import Options from '../interface/Options'
import Target from './Target'
import Equipment from './Equipment'
import Tools from '../module/Tools'
import { effectiveHitPoints, damageMitigationFromArmor, armorCap } from './FeralEHP'
import {
  averageBearAutoAttackDamage,
  maulDamage,
  sheetRageModel,
  SheetRageModelOutputs,
  swipeDamage
} from './FeralDamage'
import AttackTable from '../interface/AttackTable'
import Weights from '../interface/Weights'
import { strict as assert } from 'assert'
import Query from '../module/Query'
import ItemSlot from '../enum/ItemSlot'

import TargetType from '../enum/TargetType'

interface ComputeSheetTPSOutputs {
  sheetTPS: number
  averagedRageModelOutputs: SheetRageModelOutputs
}

interface ClassicWowLiveTankWeights {
  health: number
  armor: number
  dodge: number
}

interface RawEHPWeights {
  health: number
  armor: number
  avoidance: number
  critReduction: number
}

/// Base chance for an enemy to dodge when fighting an enemy of the same level
const BASE_ENEMY_DODGE_CHANCE = 0.05
/// Base chance for you attack to miss when attacking an enemy of the same level
const BASE_ENEMY_MISS_CHANCE = 0.05
// Elapsed time between each spell batch in Classic
const SPELL_BATCH_INTERVAL = 0.4 // secs

interface StatBuffs {
  armor?: number
  strength?: number
  agility?: number
  stamina?: number
  meleeHit?: number
  meleeCrit?: number
  attackPower?: number
  dodge?: number
  defense?: number
  haste?: number
  bonusArmor?: number
  bonusHealth?: number
  feralAttackPower?: number
}

interface OnUseBuffs {
  buffs: StatBuffs
  time: number
}

function computeOnUseUptime(
  encounterLength: number,
  activeTime: number,
  cooldown: number,
  startTime: number = 0
): number {
  const activateTimes = Tools.linspace(startTime, encounterLength, cooldown)
  const endTimes = Tools.linspace(activeTime + startTime, encounterLength, cooldown)
  if (activateTimes.length > endTimes.length) {
    endTimes.push(encounterLength)
  }
  return endTimes.map((endTime, i) => endTime - activateTimes[i]).reduce((a, b) => a + b, 0) / encounterLength
}

/**
 * A Spell cast by Character at Target.
 */
export default class Simulation {
  options: Options
  target: Target
  character: Character

  constructor(options: Options, equipment?: Equipment) {
    this.options = options

    console.log('Creating cast')
    /* By default gear is determined by Equipment(). We can override it by passing our own in.
     * If we don't pass our own equipment in */
    if (equipment === undefined) {
      equipment = new Equipment(options)
    }

    this.character = new Character(this.options.character, equipment)
    this.target = new Target(this.options.target)

    this.scoreOnUseAndSpecialItems()
  }

  get sheetTPS() {
    return this.computeSheetTPS()
  }

  get sheetDPS() {
    return this.computeSheetDPS()
  }

  // Scores on use items and items with special effects by overwriting the
  //  ItemJson.onUseThreatScore score on each
  scoreOnUseAndSpecialItems() {
    const tpsStatWeights = this.tpsStatWeights()
    let result = Query.Item({
      name: 'Manual Crowd Pummeler'
    })
    assert(result !== undefined, 'Expected to find item')
    const mcpUptime = computeOnUseUptime(this.options.encounterLength, 90, 120)
    result.onUseThreatScore = tpsStatWeights.haste * 50 * mcpUptime * Number(this.options.onUseItems)

    result = Query.Item({
      name: 'Earthstrike'
    })
    assert(result !== undefined, 'Expected to find item')
    const earthstrikeUptime = computeOnUseUptime(this.options.encounterLength, 20, 120)
    result.onUseThreatScore = tpsStatWeights.attackPower * 280 * earthstrikeUptime * Number(this.options.onUseItems)

    result = Query.Item({
      name: 'Kiss of the Spider'
    })
    assert(result !== undefined, 'Expected to find item')
    const kissOfTheSpiderUptime = computeOnUseUptime(this.options.encounterLength, 15, 120)
    result.onUseThreatScore = tpsStatWeights.haste * 20 * kissOfTheSpiderUptime * Number(this.options.onUseItems)

    result = Query.Item({
      name: `Slayer's Crest`
    })
    assert(result !== undefined, 'Expected to find item')
    result.onUseThreatScore = tpsStatWeights.attackPower * 260 * earthstrikeUptime * Number(this.options.onUseItems)

    // The 2% threat enchant effect is the AP equivalent of 2% of your total threat
    const threatEnchant = Query.Enchants({
      name: 'Enchant Gloves - Threat',
      slot: ItemSlot.Hands
    })[0]
    const tpsAPEquivalent = this.computeSheetTPS() / this.computeRawDPSStatWeights().rawTPSWeights.attackPower
    threatEnchant.onUseThreatScore = tpsAPEquivalent - tpsAPEquivalent / 1.02

    // To score the unique effect from HoJ compute TPS with and without
    //  the effect active then normalize the difference in TPS
    //  to the difference in TPS from one additional attackPower
    result = Query.Item({
      name: `Hand of Justice`
    })
    assert(result !== undefined, 'Expected to find item')
    result.onUseThreatScore =
      (this.computeSheetTPS(true) - this.computeSheetTPS(false)) /
      this.computeRawDPSStatWeights().rawTPSWeights.attackPower

    if (this.target.options.type === TargetType.Undead) {
      result = Query.Item({
        name: 'Seal of the Dawn'
      })
      assert(result !== undefined, 'Expected to find item')
      result.onUseThreatScore = this.tpsStatWeights().attackPower * 81
      result = Query.Item({
        name: 'Mark of the Champion'
      })
      assert(result !== undefined, 'Expected to find item')
      result.onUseThreatScore = this.tpsStatWeights().attackPower * 150
    } else if (this.target.options.type === TargetType.Demon) {
      result = Query.Item({
        name: 'Mark of the Champion'
      })
      assert(result !== undefined, 'Expected to find item')
      result.onUseThreatScore = this.tpsStatWeights().attackPower * 150
    } else {
      result = Query.Item({
        name: 'Seal of the Dawn'
      })
      assert(result !== undefined, 'Expected to find item')
      result.onUseThreatScore = 0
      result = Query.Item({
        name: 'Mark of the Champion'
      })
      assert(result !== undefined, 'Expected to find item')
      result.onUseThreatScore = 0
    }
  }

  ehp(dodgeWeight: number): number {
    const enemyAtkTable = this.enemyAttackTable
    const ehpFullWeight = effectiveHitPoints(
      this.character.health,
      this.character.armor,
      this.enemyAttackTable.criticalHit,
      enemyAtkTable.miss + enemyAtkTable.dodge,
      this.target.level
    )
    const ehpNoDodge = effectiveHitPoints(
      this.character.health,
      this.character.armor,
      this.enemyAttackTable.criticalHit,
      0,
      this.target.level
    )
    return ehpFullWeight * dodgeWeight + (1 - dodgeWeight) * ehpNoDodge
  }

  get averageBearAutoAttackDamage(): number {
    return averageBearAutoAttackDamage(
      this.character.attackPower(this.target.options.type),
      this.playerNormalAttackTable,
      this.character.options.talents.naturalWeaponsRank,
      this.character.saygesDarkFortuneOfDamageBonus
    )
  }

  computeRawDPSStatWeights() {
    const castClone: Simulation = Tools.CloneObject(this)
    let dpsStatWeights = {
      stamina: 0,
      armor: 0,
      agility: 0,
      defense: 0,
      dodge: 0,
      bonusArmor: 0,
      bonusHealth: 0,
      strength: 0,
      meleeCrit: 0,
      meleeHit: 0,
      attackPower: 0,
      haste: 0,
      feralAttackPower: 0
    }
    const WEIGHTS_TO_SKIP = ['stamina', 'bonusHealth', 'feralAttackPower']
    let threatStatWeights = Tools.CloneObject(dpsStatWeights)
    const currentDPS = this.computeSheetDPS()
    const currentTPS = this.computeSheetTPS()
    for (const key in threatStatWeights) {
      if (WEIGHTS_TO_SKIP.includes(key)) {
        continue
      }
      let dStat = -1
      // Melee Hit has a hard cap at 9% and at 1% and below provides zero value
      // (the first point of melee hit is ignored due to a bug in vinilla 1.12)

      // In order to still generate a weight for melee hit even when over/under cap
      // adjust the melee hit to the first value that will produce a meaningful difference
      if (key === 'meleeHit') {
        if (castClone.character.meleeHit > 0.09) {
          dStat = -castClone.character.meleeHit * 100 + 8
        } else if (castClone.character.meleeHit < 0.01) {
          dStat = -castClone.character.meleeHit * 100 + 2
        } else {
          dStat = -1
        }
      } else if (key === 'defense') {
        dStat = -1
      }
      castClone.character.equipment.artificialStatBonuses = Equipment.createArtificialItem({ [key]: dStat })
      const flip = dStat < 0 ? -1 : 1
      dpsStatWeights = Object.assign(dpsStatWeights, {
        [key]: (castClone.computeSheetDPS() - currentDPS) * flip
      })
      threatStatWeights = Object.assign(threatStatWeights, {
        [key]: (castClone.computeSheetTPS() - currentTPS) * flip
      })
    }
    dpsStatWeights.feralAttackPower = dpsStatWeights.attackPower
    threatStatWeights.feralAttackPower = threatStatWeights.attackPower
    return { rawDPSWeights: dpsStatWeights, rawTPSWeights: threatStatWeights }
  }

  overallStatWeights(): Weights {
    const weights = this.dodgeWeightedEHPStatWeights()
    for (const [key, value] of Object.entries(this.tpsStatWeights())) {
      // @ts-ignore: Typescript doesn't recognize key as a valid way to index into the Weights object
      weights[key] += value
    }
    return weights
  }

  dpsStatWeights(): Weights {
    const statWeights = this.computeRawDPSStatWeights().rawDPSWeights
    return {
      stamina: statWeights.stamina / statWeights.attackPower,
      armor: statWeights.armor / statWeights.attackPower,
      agility: statWeights.agility / statWeights.attackPower,
      defense: statWeights.defense / statWeights.attackPower,
      dodge: statWeights.dodge / statWeights.attackPower,
      bonusArmor: statWeights.bonusArmor / statWeights.attackPower,
      bonusHealth: statWeights.bonusHealth / statWeights.attackPower,
      strength: statWeights.strength / statWeights.attackPower,
      meleeCrit: statWeights.meleeCrit / statWeights.attackPower,
      meleeHit: statWeights.meleeHit / statWeights.attackPower,
      attackPower: statWeights.attackPower / statWeights.attackPower,
      haste: statWeights.haste / statWeights.attackPower,
      feralAttackPower: statWeights.feralAttackPower / statWeights.attackPower
    }
  }
  tpsStatWeights(): Weights {
    const statWeights = this.computeRawDPSStatWeights().rawTPSWeights
    return {
      stamina: statWeights.stamina / statWeights.attackPower,
      armor: statWeights.armor / statWeights.attackPower,
      agility: statWeights.agility / statWeights.attackPower,
      defense: statWeights.defense / statWeights.attackPower,
      dodge: statWeights.dodge / statWeights.attackPower,
      bonusArmor: statWeights.bonusArmor / statWeights.attackPower,
      bonusHealth: statWeights.bonusHealth / statWeights.attackPower,
      strength: statWeights.strength / statWeights.attackPower,
      meleeCrit: statWeights.meleeCrit / statWeights.attackPower,
      meleeHit: statWeights.meleeHit / statWeights.attackPower,
      attackPower: statWeights.attackPower / statWeights.attackPower,
      haste: statWeights.haste / statWeights.attackPower,
      feralAttackPower: statWeights.feralAttackPower / statWeights.attackPower
    }
  }

  ehpAPEquivalent(): number {
    return this.character.equipment.mitScore(this.dodgeWeightedEHPStatWeights())
  }

  // TODO: Make this scores from all the items (maybe still want to maximize EHP and TPS actually)
  tpsAPEquivalent(): number {
    return this.character.equipment.threatScore(this.tpsStatWeights())
  }

  overallScore(): number {
    return this.tpsAPEquivalent() + this.ehpAPEquivalent()
  }

  // EHP weights normalized to stamina
  inflatedEHPStatWeights(): Weights {
    const rawEHPStatWeights = this.computeRawInflatedEHPStatWeights()
    const agiWeight =
      rawEHPStatWeights.armor * 2 * this.character._buffStatMultiplier * this.character.agiAndEquipmentArmorModifier +
      (rawEHPStatWeights.avoidance * this.character._buffStatMultiplier) / 20
    const staminaWeight = rawEHPStatWeights.health * this.character.stamToHealthRatio()
    const armorWeight =
      rawEHPStatWeights.armor * this.character.equipmentArmorModifier * this.character.agiAndEquipmentArmorModifier
    // Defense increases your dodge by 0.04 the enemies chance to miss by 0.04 and decreases the enemies crit chance by 0.04
    const defenseWeight = 0.08 * rawEHPStatWeights.avoidance + 0.04 * rawEHPStatWeights.critReduction
    const bonusArmorWeight = rawEHPStatWeights.armor
    const bonusHealthWeight = rawEHPStatWeights.health * this.character.hpModifier
    const dodgeWeigtht = rawEHPStatWeights.avoidance
    return {
      stamina: this.options.desiredStaminaAPRatio,
      armor: (armorWeight / staminaWeight) * this.options.desiredStaminaAPRatio,
      agility: (agiWeight / staminaWeight) * this.options.desiredStaminaAPRatio,
      defense: (defenseWeight / staminaWeight) * this.options.desiredStaminaAPRatio,
      dodge: (dodgeWeigtht / staminaWeight) * this.options.desiredStaminaAPRatio,
      bonusArmor: (bonusArmorWeight / staminaWeight) * this.options.desiredStaminaAPRatio,
      bonusHealth: (bonusHealthWeight / staminaWeight) * this.options.desiredStaminaAPRatio,
      strength: 0,
      meleeCrit: 0,
      meleeHit: 0,
      attackPower: 0,
      haste: 0,
      feralAttackPower: 0
    }
  }

  // EHP weights normalized to stamina
  baselineEHPStatWeights(): Weights {
    const rawEHPStatWeights = this.computeRawBaselineEHPStatWeights()
    const agiWeight =
      rawEHPStatWeights.armor * 2 * this.character._buffStatMultiplier * this.character.agiAndEquipmentArmorModifier +
      (rawEHPStatWeights.avoidance * this.character._buffStatMultiplier) / 20
    const staminaWeight = rawEHPStatWeights.health * this.character.stamToHealthRatio()
    const armorWeight =
      rawEHPStatWeights.armor * this.character.equipmentArmorModifier * this.character.agiAndEquipmentArmorModifier
    // Defense increases your dodge by 0.04% the enemies chance to miss by 0.04% and decreases the enemies crit chance by
    const defenseWeight = 0.08 * rawEHPStatWeights.avoidance + 0.04 * rawEHPStatWeights.critReduction
    const bonusArmorWeight = rawEHPStatWeights.armor
    const bonusHealthWeight = rawEHPStatWeights.health * this.character.hpModifier
    const dodgeWeigtht = rawEHPStatWeights.avoidance
    return {
      stamina: this.options.desiredStaminaAPRatio,
      armor: (armorWeight / staminaWeight) * this.options.desiredStaminaAPRatio,
      agility: (agiWeight / staminaWeight) * this.options.desiredStaminaAPRatio,
      defense: (defenseWeight / staminaWeight) * this.options.desiredStaminaAPRatio,
      dodge: (dodgeWeigtht / staminaWeight) * this.options.desiredStaminaAPRatio,
      bonusArmor: (bonusArmorWeight / staminaWeight) * this.options.desiredStaminaAPRatio,
      bonusHealth: (bonusHealthWeight / staminaWeight) * this.options.desiredStaminaAPRatio,
      strength: 0,
      meleeCrit: 0,
      meleeHit: 0,
      attackPower: 0,
      haste: 0,
      feralAttackPower: 0
    }
  }

  dodgeWeightedEHPStatWeights(): Weights {
    const weights = this.baselineEHPStatWeights()
    const inflatedWeights = this.inflatedEHPStatWeights()
    for (const [key, value] of Object.entries(weights)) {
      // @ts-ignore: Typescript doesn't recognize key as a valid way to index into the Weights object
      weights[key] = (1 - this.options.ehpDodgeWeight) * value + this.options.ehpDodgeWeight * inflatedWeights[key]
    }
    return weights
  }

  computeClassicWowLiveTankWeights(): ClassicWowLiveTankWeights {
    const rawBaselineWeights = this.computeRawBaselineEHPStatWeights()
    const rawInflatedWeights = this.computeRawInflatedEHPStatWeights()
    const weights = this.dodgeWeightedEHPStatWeights()
    return {
      health:
        ((1 - this.options.ehpDodgeWeight) * rawBaselineWeights.health +
          this.options.ehpDodgeWeight * rawInflatedWeights.health) /
        this.character.stamToHealthRatio(),
      armor: weights.armor,
      dodge: weights.dodge
    }
  }

  // Returns the change in EHP gained from assuming zero value from dodges and misses
  //  1 hp
  //  1 armor (final armor not armor from equipment that's subject to the bear form modifier)
  //  1% reduction in chance to be critically hit
  //  1% additional avoidance (player dodge/enemy miss chance)
  computeRawBaselineEHPStatWeights(): RawEHPWeights {
    const baseEHP = effectiveHitPoints(
      this.character.health,
      this.character.armor,
      this.enemyAttackTable.criticalHit,
      0,
      this.target.level
    )
    const armorDiff = Math.max(this.character.armor - armorCap(this.target.level) + 1, 1)
    let newEHP = effectiveHitPoints(
      this.character.health,
      this.character.armor - armorDiff,
      this.enemyAttackTable.criticalHit,
      0,
      this.target.level
    )
    const armorWeight = baseEHP - newEHP
    newEHP = effectiveHitPoints(
      this.character.health,
      this.character.armor,
      this.enemyAttackTable.criticalHit + 0.01,
      0,
      this.target.level
    )
    const critReductionWeight = -(newEHP - baseEHP)
    newEHP = effectiveHitPoints(
      this.character.health + 1,
      this.character.armor,
      this.enemyAttackTable.criticalHit,
      0,
      this.target.level
    )
    const healthWeight = newEHP - baseEHP
    return {
      health: healthWeight,
      armor: armorWeight,
      avoidance: 0,
      critReduction: critReductionWeight
    }
  }

  // Returns the change in EHP gained from assuming full value from dodges and misses
  //  1 hp
  //  1 armor (final armor not armor from equipment that's subject to the bear form modifier)
  //  1% reduction in chance to be critically hit
  //  1% additional avoidance (player dodge/enemy miss chance)
  computeRawInflatedEHPStatWeights(): RawEHPWeights {
    const baseEHP = effectiveHitPoints(
      this.character.health,
      this.character.armor,
      this.enemyAttackTable.criticalHit,
      this.enemyAttackTable.miss + this.enemyAttackTable.dodge,
      this.target.level
    )
    const armorDiff = Math.max(this.character.armor - armorCap(this.target.level) + 1, 1)
    let newEHP = effectiveHitPoints(
      this.character.health,
      this.character.armor - armorDiff,
      this.enemyAttackTable.criticalHit,
      this.enemyAttackTable.miss + this.enemyAttackTable.dodge,
      this.target.level
    )
    const armorWeight = baseEHP - newEHP
    newEHP = effectiveHitPoints(
      this.character.health,
      this.character.armor,
      this.enemyAttackTable.criticalHit,
      this.enemyAttackTable.miss + this.enemyAttackTable.dodge + 0.01,
      this.target.level
    )
    const avoidanceWeight = newEHP - baseEHP
    newEHP = effectiveHitPoints(
      this.character.health,
      this.character.armor,
      this.enemyAttackTable.criticalHit + 0.01,
      this.enemyAttackTable.miss + this.enemyAttackTable.dodge,
      this.target.level
    )
    const critReductionWeight = -(newEHP - baseEHP)
    newEHP = effectiveHitPoints(
      this.character.health + 1,
      this.character.armor,
      this.enemyAttackTable.criticalHit,
      this.enemyAttackTable.miss + this.enemyAttackTable.dodge,
      this.target.level
    )
    const healthWeight = newEHP - baseEHP
    return {
      health: healthWeight,
      armor: armorWeight,
      avoidance: avoidanceWeight,
      critReduction: critReductionWeight
    }
  }

  // Returns the enemies attack table when attacking the given character
  _enemyAttackTable(character: Character): AttackTable {
    const playerDodgeChance = character.dodge
    // Any defense beyond the cap does not reduce crushign blow chance
    const chanceToBeCrushed = (this.target.level * 5 - Math.min(character.level * 5, character.defense)) * 0.02 - 0.15
    const chanceToBeCriticallyHit = Math.max(0.056 - ((character.defense - 300) * 0.04) / 100, 0)

    // Base chance a mob will miss when attacking a player
    // https://vanilla-wow.fandom.com/wiki/Miss
    // https://github.com/magey/classic-warrior/wiki/Attack-table#miss
    const BASE_MOB_MISS_CHANCE = 0.05
    const dodgeFromDefense = ((character.defense - this.target.level * 5) * 0.04) / 100
    // Defense increase both players chance to dodge AND enemy chance to miss
    const missFromDefense = dodgeFromDefense
    return new AttackTable({
      miss: BASE_MOB_MISS_CHANCE + missFromDefense - this.target.buffStatBonuses.meleeHit,
      dodge: playerDodgeChance + dodgeFromDefense,
      parry: 0,
      glancingBlow: 0,
      block: 0,
      crushingBlow: chanceToBeCrushed,
      criticalHit: chanceToBeCriticallyHit
    })
  }

  get enemyAttackTable(): AttackTable {
    return this._enemyAttackTable(this.character)
  }

  // Extra attacks per second gained from the Hand of Justice trinket
  //  e.g. returns 1.03 if you gain 0.03 attacks per second from Hand of Justice
  handOfJusticeAutoAttackModifer(attackSpeed: number): number {
    // Hand of Justice has a 2 percent chance to proc on
    // hit and it's "double batched" in that when it procs it
    // doesn't cause two attacks to occur in the same batch
    // but the second attack will come out the very next spell batch
    return (
      1 +
      (1 - SPELL_BATCH_INTERVAL / attackSpeed) *
        0.02 *
        (this.playerNormalAttackTable.ordinaryHit +
          this.playerNormalAttackTable.criticalHit +
          this.playerNormalAttackTable.glancingBlow)
    )
  }

  // Returns DPS if you mauled every auto-attack and cast nothing else (assumes you have infinite rage)
  maulSheetDPS(handOfJusticeOverride: boolean | undefined = undefined): number {
    const attackSpeed = this.character.sheetWeaponSpeed()
    const hasHandOfJustEquipped =
      handOfJusticeOverride === undefined
        ? this.character.equipment.trinket.name === 'Hand of Justice' ||
          this.character.equipment.trinket2.name === 'Hand of Justice'
        : handOfJusticeOverride
    return (
      (this.averageMaulDamage / attackSpeed) *
      (hasHandOfJustEquipped ? this.handOfJusticeAutoAttackModifer(attackSpeed) : 1)
    )
  }

  // Returns DPS if you were only auto-attacking
  autoAttackSheetDPS(handOfJusticeOverride: boolean | undefined = undefined): number {
    const attackSpeed = this.character.sheetWeaponSpeed()
    const hasHandOfJustEquipped =
      handOfJusticeOverride === undefined
        ? this.character.equipment.trinket.name === 'Hand of Justice' ||
          this.character.equipment.trinket2.name === 'Hand of Justice'
        : handOfJusticeOverride
    return (
      (this.averageBearAutoAttackDamage / this.character.sheetWeaponSpeed()) *
      (hasHandOfJustEquipped ? this.handOfJusticeAutoAttackModifer(attackSpeed) : 1)
    )
  }

  // Computes DPS from a bear's full damage rotation (maul/swipe/auto-attacks)
  //  Assumes you are attacking a stationary target with no mechanics (e.g. target dummy)
  //  Models rage and assumes you will always prioritize maul before swipe
  //  Assumes you use onUseItems on cooldown
  //  Does not model globals spent keeping feral faerie fire up or other globals like demo roar
  // TODO: Add check to see if it is better to swipe than maul
  computeSheetDPS(): number {
    const rageInfo = this.rageModel()

    const dpsComputation = []
    let currentBuffs: any = {}
    let lastTime = 0
    const elapsedTimes: Array<number> = []
    for (const onUseEvent of this.computeTemporaryOnUseBuffs()) {
      currentBuffs = Tools.sumObjectsByKey(currentBuffs, onUseEvent.buffs as Record<string, number>)
      this.character.equipment.onUseStatBonuses = Equipment.createArtificialItem({ ...currentBuffs })
      const rageModelResults = this.rageModel()
      dpsComputation.push(
        rageInfo.maulUptime * this.maulSheetDPS() +
          (1 - rageInfo.maulUptime) * this.autoAttackSheetDPS() +
          this.averageSwipeDamage * Math.min(this.target.options.numTargets, 3) * rageModelResults.swipesPerSecond
      )
      elapsedTimes.push(onUseEvent.time - lastTime)
      lastTime = onUseEvent.time
    }
    // console.debug(`DPS Calculations: ${JSON.stringify(dpsComputation)}`)
    // console.debug(`Times Calculations: ${JSON.stringify(elapsedTimes)}`)
    const m = Tools.weightedMean(dpsComputation.slice(0, dpsComputation.length - 1), elapsedTimes.slice(1))
    // console.debug(`Wighted DPS: ${m}`)

    // Againist a target with full armor shred assume 80dps added while active
    // TODO: Model armor shred
    // https://docs.google.com/spreadsheets/d/1i85Vo-C6sAtGj57LwhBkFed8_jTS6CjEp_Y0yoxBbz0/edit#gid=0
    let tentacleDPS = 0
    if (this.character.equipment.trinket.name === `Vanquished Tentacle of C'thun` && this.options.onUseItems) {
      tentacleDPS = computeOnUseUptime(this.options.encounterLength, 30, 180) * 80
    } else if (
      this.character.equipment.trinket.hasOnUse &&
      this.character.equipment.trinket2.name === `Vanquished Tentacle of C'thun` &&
      this.options.onUseItems
    ) {
      tentacleDPS = computeOnUseUptime(this.options.encounterLength, 30, 180, 20) * 80
    }

    this.character.equipment.onUseStatBonuses = Equipment.createArtificialItem({})
    return m + tentacleDPS
  }

  // Returns times and changes in stats when onUse items activate and de-activate
  computeTemporaryOnUseBuffs(): Array<OnUseBuffs> {
    let onUseTrinket = ''
    let onUseWeapon = ''
    // All these trinkets share a cooldown so if one is activated
    //  they both go on cooldown
    const SHARED_COOLDOWN_TRINKET_SET = [
      'Earthstrike',
      'Jom Gabbar',
      `Slayer's Crest`,
      'Kiss of the Spider',
      'Zandalarian Hero Medallion'
    ]
    const events: Array<OnUseBuffs> = []
    if (
      this.character.equipment.trinket.hasOnUse &&
      SHARED_COOLDOWN_TRINKET_SET.includes(this.character.equipment.trinket.name)
    ) {
      onUseTrinket = this.character.equipment.trinket.name
    } else if (
      this.character.equipment.trinket2.hasOnUse &&
      SHARED_COOLDOWN_TRINKET_SET.includes(this.character.equipment.trinket2.name)
    ) {
      onUseTrinket = this.character.equipment.trinket2.name
    }
    if (this.character.equipment.mainhand.hasOnUse) {
      onUseWeapon = this.character.equipment.mainhand.name
    }
    if (!this.options.onUseItems || (onUseWeapon === '' && onUseTrinket === '')) {
      return [
        { time: 0, buffs: {} },
        { time: this.options.encounterLength, buffs: {} }
      ]
    }
    if (onUseWeapon === 'Manual Crowd Pummeler') {
      const activateTimes = Tools.linspace(0, this.options.encounterLength, 120)
      const endTimes = Tools.linspace(90, this.options.encounterLength, 120)
      for (const t of activateTimes) {
        events.push({ time: t, buffs: { haste: 50 } })
      }
      for (const t of endTimes) {
        events.push({ time: t, buffs: { haste: -50 } })
      }
    }

    // Trinket 1
    if (onUseTrinket === 'Earthstrike') {
      const activateTimes = Tools.linspace(0, this.options.encounterLength, 120)
      const endTimes = Tools.linspace(20, this.options.encounterLength, 120)
      for (const t of activateTimes) {
        events.push({ time: t, buffs: { attackPower: 280 } })
      }
      for (const t of endTimes) {
        events.push({ time: t, buffs: { attackPower: -280 } })
      }
    }
    if (onUseTrinket === `Slayer's Crest`) {
      const activateTimes = Tools.linspace(0, this.options.encounterLength, 120)
      const endTimes = Tools.linspace(20, this.options.encounterLength, 120)
      for (const t of activateTimes) {
        events.push({ time: t, buffs: { attackPower: 260 } })
      }
      for (const t of endTimes) {
        events.push({ time: t, buffs: { attackPower: -260 } })
      }
    }
    if (onUseTrinket === `Kiss of the Spider`) {
      // Haste stacks multiplicitively. Haste + mcp comes out to +80% attack speed
      //  Since both one use timers line up we assume that both will be
      //  active at the same time if both are equipped
      const kissHasteAmount = onUseWeapon === 'Manual Crowd Pummeler' ? 30 : 20
      const activateTimes = Tools.linspace(0, this.options.encounterLength, 120)
      const endTimes = Tools.linspace(15, this.options.encounterLength, 120)
      for (const t of activateTimes) {
        events.push({ time: t, buffs: { haste: kissHasteAmount } })
      }
      for (const t of endTimes) {
        events.push({ time: t, buffs: { haste: -kissHasteAmount } })
      }
    }
    if (events.length === 0) {
      events.push({ time: 0, buffs: {} })
      events.push({ time: this.options.encounterLength, buffs: {} })
    } else {
      events.push({ time: this.options.encounterLength, buffs: {} })
    }
    events.sort((a, b) => a.time - b.time)
    const combinedEvents: Array<OnUseBuffs> = []
    for (const onUseEvent of events) {
      if (combinedEvents.map((e) => e.time).includes(onUseEvent.time)) {
        continue
      }
      const eventsAtCurrentTime = events.filter((e) => e.time === onUseEvent.time)
      combinedEvents.push({
        time: onUseEvent.time,
        buffs: Tools.sumObjectsByKey(...(eventsAtCurrentTime.map((e) => e.buffs) as Array<Record<string, number>>))
      })
    }

    return combinedEvents
  }

  computeSheetTPS(handOfJusticeOverride: boolean | undefined = undefined): number {
    return this._computeSheetTPS(handOfJusticeOverride).sheetTPS
  }

  // Rage model averaged over all on use effects
  averagedRageModel(): SheetRageModelOutputs {
    return this._computeSheetTPS().averagedRageModelOutputs
  }

  _computeSheetTPS(handOfJusticeOverride: boolean | undefined = undefined): ComputeSheetTPSOutputs {
    const tpsComputations = []
    let currentBuffs: any = {}
    let lastTime = 0
    const elapsedTimes: Array<number> = []
    const rageModelEvaluations: Array<SheetRageModelOutputs> = []
    for (const onUseEvent of this.computeTemporaryOnUseBuffs()) {
      currentBuffs = Tools.sumObjectsByKey(currentBuffs, onUseEvent.buffs as Record<string, number>)
      this.character.equipment.onUseStatBonuses = Equipment.createArtificialItem({ ...currentBuffs })
      const THREAT_GENERATED_FROM_ONE_PT_OF_RAGE_GEN = 5
      const rageModelResults = this.rageModel()
      rageModelEvaluations.push(rageModelResults)
      // Rage generated from primal fury crits is unaffected by the bear form threat modifers
      // https://discordapp.com/channels/253205420225724416/253206574787461120/741735841696710771
      // TODO: Test with the threat glove enchanct
      tpsComputations.push(
        rageModelResults.maulUptime * this.character.maulThreatModifier * this.maulSheetDPS(handOfJusticeOverride) +
          (1 - rageModelResults.maulUptime) *
            this.character.baseThreatModifier *
            this.autoAttackSheetDPS(handOfJusticeOverride) +
          this.averageSwipeDamage *
            Math.min(this.target.options.numTargets, 3) *
            this.character.swipeThreatModifier *
            rageModelResults.swipesPerSecond +
          rageModelResults.ragePerSecFromAutoAttacks * THREAT_GENERATED_FROM_ONE_PT_OF_RAGE_GEN +
          rageModelResults.ragePerSecFromSwipe * THREAT_GENERATED_FROM_ONE_PT_OF_RAGE_GEN
      )
      elapsedTimes.push(onUseEvent.time - lastTime)
      lastTime = onUseEvent.time
    }
    // Computed the weighted average of all the TPS calcutions with different on use items average
    // where the weights are uptime
    const m = Tools.weightedMean(tpsComputations.slice(0, tpsComputations.length - 1), elapsedTimes.slice(1))
    const averagedSheetRageModelOutputs: SheetRageModelOutputs = {
      ragePerSecFromAutoAttacks: 0,
      ragePerSecFromSwipe: 0,
      ragePerSecondFromDamageTaken: 0,
      swipesPerSecond: 0,
      maulUptime: 0,
      swipeUptime: 0
    }
    // Computed the weighted average of all rage model calculations
    // where the weights are uptime
    for (const property in averagedSheetRageModelOutputs) {
      const vals: Array<number> = []
      for (const rageModelOutputs of rageModelEvaluations) {
        // @ts-ignore
        vals.push(rageModelOutputs[property])
      }
      // @ts-ignore
      averagedSheetRageModelOutputs[property] = Tools.weightedMean(
        vals.slice(0, vals.length - 1),
        elapsedTimes.slice(1)
      )
    }

    this.character.equipment.onUseStatBonuses = Equipment.createArtificialItem({})
    return { sheetTPS: m, averagedRageModelOutputs: averagedSheetRageModelOutputs }
  }

  get playerMeleeHitCap(): number {
    return BASE_ENEMY_MISS_CHANCE + 0.01 + (this.target.level - this.character.level) * 0.01
  }

  // https://github.com/magey/classic-warrior/wiki/Attack-table#dodge
  get playerNormalAttackTable(): AttackTable {
    /// https://vanilla-wow.fandom.com/wiki/Miss
    const basePlayerMissChance = BASE_ENEMY_MISS_CHANCE + (this.target.level - this.character.level) * 0.01
    // 1.12 change where the first point of hit is ignored
    const playerMissChance = Math.max(basePlayerMissChance - Math.max(this.character.meleeHit - 0.01, 0), 0)
    const enemyDodgeChance = BASE_ENEMY_DODGE_CHANCE + Math.max((this.target.level - this.character.level) * 0.005, 0)
    const BASE_PARRY_CHACNCE = 0.14
    const enemyParryChance = BASE_PARRY_CHACNCE // + Math.max((this.target.defenseSkill - this.character.weaponSkill) * 0.004, 0)
    const BASE_GLANCING_BLOW_CHANCE = 0.1
    const playerGlancingBlowChance =
      this.target.level >= this.character.level
        ? BASE_GLANCING_BLOW_CHANCE + Math.max((this.target.defenseSkill - this.character.weaponSkill) * 0.02, 0)
        : 0

    const playerCrushingBlowChance = 0
    const enemyBlockChance = 0
    // https://github.com/magey/classic-warrior/wiki/Attack-table#critical-strike
    // - Players have a 1% reduced chance to crit a mob for every level it is above you
    // - When fighting a level 63 mob you lose a flat 1.8% of the crit you gain from bonus
    //    auras like Mongoose (Since in raids you will almost certainly have this much bonus
    //    crit always apply this reduction_
    let playerCritChance = this.character.meleeCrit - Math.max((this.target.level - this.character.level) * 0.01, 0)
    if (this.target.level - this.character.level >= 3) {
      playerCritChance -= 0.018
    }
    return new AttackTable({
      miss: playerMissChance,
      dodge: enemyDodgeChance,
      parry: enemyParryChance,
      glancingBlow: playerGlancingBlowChance,
      block: enemyBlockChance,
      criticalHit: playerCritChance,
      crushingBlow: playerCrushingBlowChance
    })
  }

  get damageReductionFromArmor(): number {
    return damageMitigationFromArmor(this.character.armor, this.target.level)
  }

  get bossDamageReductionFromArmor(): number {
    return damageMitigationFromArmor(this.target.armor, this.character.level)
  }

  get mitigatedDamageTakenPerSecond(): number {
    // Don't model crushing blows when computing rage from damage take to align with Nerd's sheet
    // May want to re-visit in the future
    const enemyAtkTable = this.enemyAttackTable
    enemyAtkTable.crushingBlow = 0
    return (
      this.options.unmitagatedDamageTakenPerSecond * (1 - this.damageReductionFromArmor) * enemyAtkTable.attackModifier
    )
  }

  rageModel(): SheetRageModelOutputs {
    const swingsPerSecond = 1 / this.character.sheetWeaponSpeed()
    return sheetRageModel({
      mitigatedDTPS: this.mitigatedDamageTakenPerSecond,
      maulRageCost: this.character.maulRageCost,
      swipeRageCost: this.character.swipeRageCost,
      playerAttackTable: this.playerSpecialAttackTable,
      swingsPerSecond: swingsPerSecond,
      primalFuryRank: this.character.options.talents.primalFuryRank,
      autoAttackDPS: this.averageBearAutoAttackDamage / swingsPerSecond
    })
  }

  get playerSpecialAttackTable(): AttackTable {
    // https://github.com/magey/classic-warrior/wiki/Attack-table#critical-strike
    // - Players have a 1% reduced chance to crit a mob for every level it is above you
    // - When fighting a level 63 mob you lose a flat 1.8% of the crit you gain from bonus
    //    auras like Mongoose (Since in raids you will almost certainly have this much bonus
    //    crit always apply this reduction_
    let playerCritChance = this.character.meleeCrit - Math.max((this.target.level - this.character.level) * 0.01, 0)
    if (this.target.level - this.character.level >= 3) {
      playerCritChance -= 0.018
    }
    const baseAttackTable = this.playerNormalAttackTable
    const playerGlancingBlowChance = 0
    return new AttackTable({
      miss: baseAttackTable.miss,
      dodge: baseAttackTable.dodge,
      parry: baseAttackTable.parry,
      glancingBlow: playerGlancingBlowChance,
      block: baseAttackTable.block,
      criticalHit: playerCritChance,
      crushingBlow: baseAttackTable.crushingBlow
    })
  }

  get averageSwipeDamage(): number {
    return (
      swipeDamage({
        playerAttackTable: this.playerSpecialAttackTable,
        naturalWeaponTalentPts: this.character.options.talents.naturalWeaponsRank,
        savageFuryTalentPts: this.character.options.talents.savageFuryRank,
        worldBuffDamageMultiplier: this.character.saygesDarkFortuneOfDamageBonus
      }) *
      (1 - this.bossDamageReductionFromArmor)
    )
  }

  get averageMaulDamage(): number {
    return (
      maulDamage({
        attackPower: this.character.attackPower(this.target.options.type),
        playerAttackTable: this.playerSpecialAttackTable,
        naturalWeaponTalentPts: this.character.options.talents.naturalWeaponsRank,
        savageFuryTalentPts: this.character.options.talents.savageFuryRank,
        worldBuffDamageMultiplier: this.character.saygesDarkFortuneOfDamageBonus
      }) *
      (1 - this.bossDamageReductionFromArmor)
    )
  }

  toJSON() {
    const proto = Object.getPrototypeOf(this)
    const jsonObj: any = Object.assign({}, this)

    Object.entries(Object.getOwnPropertyDescriptors(proto))
      .filter(([key, descriptor]) => typeof descriptor.get === 'function')
      .map(([key, descriptor]) => {
        if (descriptor && key[0] !== '_') {
          // try {
          const val = (this as any)[key]
          jsonObj[key] = val
          // } catch (error) {
          //  console.error(`Error calling getter ${key}`, error)
          // }
        }
      })

    return jsonObj
  }
}
