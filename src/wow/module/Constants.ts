/* constants */
import TargetType from '../enum/TargetType'
import PlayableRace from '../enum/PlayableRace'
import PlayableClass from '../enum/PlayableClass'
import Gender from '../enum/Gender'
import Options from '../interface/Options'
import ItemSlot from '../enum/ItemSlot'

const defaults: Options = {
  debug: false,
  experimental: false,
  phase: 6,
  raids: true,
  worldBosses: true,
  randomEnchants: true,
  onUseItems: true,
  encounterLength: 100,
  desiredStaminaAPRatio: 2.5,
  unmitagatedDamageTakenPerSecond: 1500,
  ehpDodgeWeight: 0.2,
  itemSearchSlot: ItemSlot.None,
  enchantSearchSlot: ItemSlot.None,
  character: {
    level: 60,
    gender: Gender.Male,
    race: PlayableRace.NightElf,
    class: PlayableClass.Druid,
    pvpRank: 1,
    talents: {
      naturesGraceRank: 1,
      moonFuryRank: 5,
      vengeanceRank: 5,
      improvedWrathRank: 5,
      improvedStarfireRank: 5,
      improvedMoonfireRank: 5,
      reflectionRank: 3,
      thickHideRank: 2,
      naturalWeaponsRank: 3,
      savageFuryRank: 2,
      predatoryStrikesRank: 3,
      heartOfTheWildRank: 5,
      primalFuryRank: 2,
      sharpenedClawsRank: 3,
      feralInstinctRank: 5,
      feralAgressionRank: 0,
      ferocityRank: 5,
      furorRank: 5,
      improvedEnrageRank: 0,
      leaderOfThePackRank: 1
    },
    buffs: [
      'RallyingCryOfTheDragonSlayer',
      'WarchiefsBlessing',
      'SongflowerSerenade',
      'BlessingOfKings',
      'GiftOfTheWild',
      'SpiritOfZandalar',
      'JujuPower',
      'MongooseElixir',
      'WinterfallFirewater',
      'DesertDumplings',
      'SpiritOfZanza',
      'GreaterStoneshieldPotion',
      'TrueshotAura',
      'BattleShout',
      'PrayerOfFortitude',
      'DevotionAura',
      'BlessingOfMight',
      'MoldarMoxie',
      'FengusFerocity'
    ],
    lockedItems: {
      head: '',
      hands: '',
      neck: '',
      waist: '',
      shoulder: '',
      legs: '',
      back: '',
      feet: '',
      chest: '',
      wrist: '',
      finger: '',
      finger2: '',
      mainhand: '',
      offhand: '',
      trinket: '',
      trinket2: '',
      idol: ''
    },
    lockedEnchants: {
      head: '',
      hands: '',
      shoulder: '',
      legs: '',
      back: '',
      feet: '',
      chest: '',
      wrist: '',
      mainhand: ''
    }
  },
  target: {
    level: 63,
    debuffs: ['ExposeArmor', 'FaerieFire', 'CurseOfRecklessness'],
    type: TargetType.Dragonkin,
    baseArmor: 3731,
    numTargets: 1
  }
}

const ATTRIBUTE_LIST = [
  'armor',
  'strength',
  'agility',
  'stamina',
  'meleeHit',
  'meleeCrit',
  'attackPower',
  'dodge',
  'defense',
  'haste',
  'bonusArmor',
  'bonusHealth',
  'feralAttackPower'
]

export default {
  globalCoolDown: 1.5,
  playerLevelCap: 60,
  spellHitCap: 16,
  spellCritCap: 100,
  /**
   * At level 60, caster classes have some expected amount of Int that will put them at 5% spell crit.
   * For example, to have 5% crit at 60 a mage needs 286 Int.  A 60 mage also needs 59.5 int to gain
   * 1 additional spell crit.  286/59.5=4.8067 which is less than 5, meaning mages have a base spell
   * crit of 5-(286/59.5)=0.1933. Likewise, a Shaman needs 160 int @ 60 for 5% crit, and 59.2 int for
   * 1 crit.  160/59.2=2.703 -> 5-(160/59.2)=2.2973 base spell crit
   *
   * http://blue.cardplace.com/cache/wow-mage/1009382.htm
   * http://blue.cardplace.com/cache/wow-general/8532087.htm
   * http://blue.cardplace.com/cache/wow-mage/559324.htm
   *
   */

  baseSpellCrit: 1.8,
  baseSpellCritMultiplier: 1.5,
  ATTRIBUTE_LIST: ATTRIBUTE_LIST,
  defaults: defaults
}
