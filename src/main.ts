import './styles/custom.scss'

import Vue from 'vue'
import Buefy from 'buefy'
import App from './App.vue'

import { FontAwesomeIcon, FontAwesomeLayers, FontAwesomeLayersText } from '@fortawesome/vue-fontawesome'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faGitlab, faDiscord } from '@fortawesome/free-brands-svg-icons'
import {
  faFilePdf,
  faFileExport,
  faBook,
  faBug,
  faFlask,
  faLock,
  faUnlock,
  faUserTimes,
  faTshirt,
  faLink,
  faBan,
  faRedo,
  faUndo,
  IconDefinition
} from '@fortawesome/free-solid-svg-icons'

Vue.config.productionTip = false

Vue.use(Buefy)

Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.component('font-awesome-layers', FontAwesomeLayers)
Vue.component('font-awesome-layers-text', FontAwesomeLayersText)

// Typescript error `Argument of type 'IconDefinition' is not assignable to parameter of type 'IconDefinitionOrPack'` gets thrown here without the cast
// Seems like some issue with the Font-Awesome library
// https://github.com/FortAwesome/Font-Awesome/issues/12575
library.add(faGitlab as IconDefinition)
library.add(faDiscord as IconDefinition)
library.add(faUndo)
library.add(faBook)
library.add(faFilePdf)
library.add(faFileExport)
library.add(faBug)
library.add(faFlask)
library.add(faLock)
library.add(faUnlock)
library.add(faUserTimes)
library.add(faTshirt)
library.add(faLink)
library.add(faBan)
library.add(faRedo)

new Vue({
  render: (h) => h(App)
}).$mount('#app')
