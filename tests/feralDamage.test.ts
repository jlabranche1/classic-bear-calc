import { averageBearAutoAttackDamage, maulDamage, computeAttackTimes, feralSim } from '../src/wow/class/FeralDamage'
import AttackTable from '../src/wow/interface/AttackTable'

test('Test Maul Damage Calc', () => {
  const attackPower = 100
  const playerAttackTable = new AttackTable({
    miss: 0,
    dodge: 0,
    parry: 0.14,
    glancingBlow: 0,
    block: 0,
    criticalHit: 0.3,
    crushingBlow: 0
  })
  const naturalWeaponTalentPts = 5
  const savageFuryTalentPts = 2
  const worldBuffDamageMultiplier = 1.1

  expect(
    maulDamage({
      attackPower: attackPower,
      playerAttackTable: playerAttackTable,
      naturalWeaponTalentPts: naturalWeaponTalentPts,
      savageFuryTalentPts: savageFuryTalentPts,
      worldBuffDamageMultiplier: worldBuffDamageMultiplier
    })
  ).toBeGreaterThan(1)
})
